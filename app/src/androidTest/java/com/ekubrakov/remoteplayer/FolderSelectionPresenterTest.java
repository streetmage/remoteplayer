package com.ekubrakov.remoteplayer;

import android.support.test.runner.AndroidJUnit4;

import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.entities.Folder;
import com.ekubrakov.remoteplayer.models.FolderSelectionModel;
import com.ekubrakov.remoteplayer.presenter.FolderSelectionPresenter;
import com.ekubrakov.remoteplayer.view_interface.FolderSelectionView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;

/**
 * Created by ekubrakov on 11.08.17.
 */

@RunWith(AndroidJUnit4.class)
public class FolderSelectionPresenterTest {

    @Mock
    FolderSelectionView view;
    @Mock
    FolderSelectionModel model;

    FolderSelectionPresenter presenter;

    @Captor
    ArgumentCaptor<DataService.GetFoldersListener> getFoldersListener;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = spy(new FolderSelectionPresenter(model, true));
        presenter.bindView(view);
    }

    @Test
    public void test_onViewSetup() {

        // when
        presenter.onViewSetup();

        // then
        then(view).should().hideHomeAsUp();

    }

    @Test
    public void test_onViewSetup_in_progress_state() {

        // when
        presenter.loadData();
        presenter.onViewSetup();

        // then
        then(view).should(times(2)).showProgressIndicator(true);
    }

    @Test
    public void test_onForcedDataReload() {

        // when
        presenter.onForcedDataReload();

        // then
        then(presenter).should().loadData();
        then(view).should().showProgressIndicator(true);
    }

    @Test
    public void test_onFolderSelected() {

        // when
        presenter.onFolderSelected(new Folder());

        // then
        then(view).should().showNewFolderSelectionConfirmationAlert();
    }

    @Test
    public void test_onNewFolderSelectionConfirmed() {

        // given
        Folder folder = new Folder();
        presenter.onFolderSelected(folder);

        // when
        presenter.onNewFolderSelectionConfirmed();

        // then
        then(model).should().setFolderForAccount(folder);
        then(view).should().openNewPlaylist();

    }

    @Test
    public void test_onNewFolderSelectionCancelled() {

        // when
        presenter.onNewFolderSelectionCancelled();

        // then
        then(view).shouldHaveZeroInteractions();
    }

    @Test
    public void test_loadData_on_success() {

        //given
        List<Folder> folders = new ArrayList();
        Folder folder1 = new Folder();
        folders.add(folder1);
        Folder folder2 = new Folder();
        folders.add(folder2);
        Folder[] folderArray = folders.toArray(new Folder[folders.size()]);

        // when
        presenter.loadData();

        // then
        then(model).should().getFolders(getFoldersListener.capture());
        getFoldersListener.getValue().onSuccess(folders);
        then(view).should().showProgressIndicator(false);

        // when
        presenter.onViewSetup();

        // then
        then(view).should(times(2)).updateFolders(folderArray);
    }

    @Test
    public void test_loadData_on_fail() {

        // when
        presenter.loadData();

        // then
        then(model).should().getFolders(getFoldersListener.capture());
        getFoldersListener.getValue().onFailure(0, new String());
        then(view).should().showErrorMessage(any(String.class));
        then(view).should().showProgressIndicator(false);
    }

}
