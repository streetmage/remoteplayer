package com.ekubrakov.remoteplayer;

import android.support.test.runner.AndroidJUnit4;

import com.ekubrakov.remoteplayer.presenter.LogInPresenter;
import com.ekubrakov.remoteplayer.view_interface.LogInView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;

/**
 * Created by ekubrakov on 09.08.17.
 */

@RunWith(AndroidJUnit4.class)
public class LogInPresenterTest {

    @Spy
    LogInPresenter presenter;
    @Mock
    LogInView view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter.bindView(view);
    }

    @Test
    public void test_onLogInButtonClick() throws Exception {

        // when
        presenter.onLogInButtonClick();

        // then
        then(view).should().openOAuthBrowser();
    }

    @Test
    public void test_onAuthorizationFailed() throws Exception {

        // when
        presenter.onAuthorizationFailed();

        // then
        then(view).should().showAuthorizationFailedMessage();
    }
}
