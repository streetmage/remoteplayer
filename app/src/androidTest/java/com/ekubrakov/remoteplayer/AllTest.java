package com.ekubrakov.remoteplayer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by ekubrakov on 10.08.17.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        LogInPresenterTest.class,
        OAuthBrowserPresenterTest.class,
        FolderSelectionPresenterTest.class})
public class AllTest {
}
