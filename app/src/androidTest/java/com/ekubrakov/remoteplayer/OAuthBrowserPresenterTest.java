package com.ekubrakov.remoteplayer;

import android.net.Uri;
import android.support.test.runner.AndroidJUnit4;

import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.entities.Account;
import com.ekubrakov.remoteplayer.models.OAuthBrowserModel;
import com.ekubrakov.remoteplayer.presenter.OAuthBrowserPresenter;
import com.ekubrakov.remoteplayer.view_interface.OAuthBrowserView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

/**
 * Created by ekubrakov on 10.08.17.
 */

@RunWith(AndroidJUnit4.class)
public class OAuthBrowserPresenterTest {

    @Spy
    @InjectMocks
    OAuthBrowserPresenter presenter;
    @Mock
    OAuthBrowserView view;
    @Mock
    OAuthBrowserModel model;
    @Captor
    ArgumentCaptor<DataService.AccountInfoListener> accountInfoListener;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter.bindView(view);
        accountInfoListener = ArgumentCaptor.forClass(DataService.AccountInfoListener.class);
    }

    @Test
    public void test_onViewSetup_when_state_is_IDLE() throws Exception {

        // given
        String authorizationUrl = "https://api.yandex.com/?params=params";
        given(model.getAuthorizationUrl()).willReturn(authorizationUrl);

        // when
        presenter.onViewSetup();

        // then
        then(view).should().openOAuthDialog(model.getAuthorizationUrl() + "&state=" + presenter.getGeneratedState());
    }

    @Test
    public void test_onShouldOverrideUrlLoading_has_access_token() throws Exception {

        // given
        String accessToken = "test_token";

        // when
        presenter.onViewSetup();
        String state = presenter.getGeneratedState();
        Uri uri = Uri.parse("remoteplayer://callback#access_token=" + accessToken + "&state=" + state);
        presenter.onShouldOverrideUrlLoading(uri);

        // then
        then(model).should().setAccessToken(accessToken);
        then(view).should().showProgressIndicator(true);
    }

    @Test
    public void test_onShouldOverrideUrlLoading_no_access_token() throws Exception {

        // when
        presenter.onViewSetup();
        String state = presenter.getGeneratedState();
        Uri uri = Uri.parse("remoteplayer://callback#state=" + state);
        presenter.onShouldOverrideUrlLoading(uri);

        // then
        then(view).should().returnToLogIn();
    }

    @Test
    public void test_loadAccountInfo_on_success() throws Exception {

        // given
        Account account = new Account();
        String accountId = "test_account_id";
        account.setAccountId(accountId);

        // when
        presenter.loadAccountInfo();

        // then
        then(model).should().getAccountInfo(eq(null), accountInfoListener.capture());
        accountInfoListener.getValue().onSuccess(account);
        then(view).should().showProgressIndicator(false);
        then(view).should().openFolderSelection();
        then(model).should().setAccountId(accountId);
    }

    @Test
    public void test_loadAccountInfo_on_fail() throws Exception {

        // given
        String errorMessage = "error";

        // when
        presenter.loadAccountInfo();

        // then
        then(model).should().getAccountInfo(eq(null), accountInfoListener.capture());
        ;
        accountInfoListener.getValue().onFailure(0, errorMessage);
        then(view).should().showProgressIndicator(false);
        then(view).should().showErrorMessage(errorMessage);
    }

}
