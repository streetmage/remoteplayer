package com.ekubrakov.remoteplayer.helpers;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.ekubrakov.remoteplayer.presenter.BasePresenter;

/**
 * Created by ekubrakov on 08.08.17.
 */

public class PresenterFragment extends Fragment {

    private BasePresenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public <P extends BasePresenter> P getPresenter() {
        return (P) mPresenter;
    }

    public <P extends BasePresenter> void setPresenter(P presenter) {
        mPresenter = presenter;
    }

    public boolean isPresenterSet() {
        return mPresenter != null;
    }
}
