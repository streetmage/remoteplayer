package com.ekubrakov.remoteplayer.helpers;

import android.content.Context;
import android.os.Handler;

/**
 * Created by ekubrakov on 02.08.17.
 */

public class RecurrentTimer {

    /// ============================================================================================
    /// Properties Methods
    /// ============================================================================================

    Handler mHandler;
    Runnable mRunnable;
    Listener mListener;

    boolean mStarted;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public RecurrentTimer() {
        mHandler = new Handler();
    }

    public void start() {

        if (mStarted) {
            return;
        }

        if (mRunnable == null) {
            mRunnable = new Runnable();
        }

        mHandler.post(mRunnable);
        mStarted = true;
    }

    public void stop() {
        mHandler.removeCallbacks(mRunnable);
        mStarted = false;
    }

    public Listener getListener() {
        return mListener;
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    /// ============================================================================================
    /// Runnable
    /// ============================================================================================

    private class Runnable implements java.lang.Runnable {
        @Override
        public void run() {
            if (mListener != null) {
                mListener.onTick(RecurrentTimer.this);
            }
            mHandler.postDelayed(mRunnable, 1000);
        }
    }

    /// ============================================================================================
    /// Listener
    /// ============================================================================================

    public interface Listener {
        void onTick(RecurrentTimer recurrentTimer);
    }
}
