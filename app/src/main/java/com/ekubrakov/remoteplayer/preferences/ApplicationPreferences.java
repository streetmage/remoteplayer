package com.ekubrakov.remoteplayer.preferences;

import android.content.Context;
import android.util.Log;

import com.prashantsolanki.secureprefmanager.SecurePrefManager;
import com.prashantsolanki.secureprefmanager.SecurePrefManagerInit;

/**
 * Created by ekubrakov on 27.06.17.
 */

public final class ApplicationPreferences {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    private static final String ACCESS_TOKEN_KEY = "access_token";
    private static final String ACCOUNT_ID_KEY = "account_id";

    private static ApplicationPreferences sApplicationPreferences = null;

    private Context mContext;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public static ApplicationPreferences getInstance() {
        if (sApplicationPreferences == null) {
            sApplicationPreferences = new ApplicationPreferences();
        }
        return sApplicationPreferences;
    }

    public void setContext(Context context) {
        mContext = context;
        new SecurePrefManagerInit.Initializer(mContext)
                .useEncryption(true)
                .initialize();
    }

    public String getAccessToken() {
        String accessToken = SecurePrefManager.with(mContext)
                .get(ACCESS_TOKEN_KEY)
                .defaultValue(new String())
                .go();
        return accessToken;
    }

    public void setAccessToken(String accessToken) {

        if (accessToken == null) {
            Log.d("Debug", "Access Token cannot be null");
            return;
        }

        SecurePrefManager.with(mContext)
                .set(ACCESS_TOKEN_KEY)
                .value(accessToken)
                .go();
    }

    public void removeAccessToken() {
        SecurePrefManager.with(mContext)
                .remove(ACCESS_TOKEN_KEY)
                .confirm();
    }

    public Boolean isLoggedIn() {
        String accessToken = getAccessToken();
        String accountId = getAccountId();
        return accessToken != null && accessToken.length() > 0
                && accountId != null && accountId.length() > 0;
    }

    public String getAccountId() {
        String accountId = SecurePrefManager.with(mContext)
                .get(ACCOUNT_ID_KEY)
                .defaultValue(new String())
                .go();
        return accountId;
    }

    public void setAccountId(String accountId) {

        if (accountId == null) {
            Log.d("Debug", "Account Id cannot be null");
            return;
        }

        SecurePrefManager.with(mContext)
                .set(ACCOUNT_ID_KEY)
                .value(accountId)
                .go();
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private ApplicationPreferences() {
        //
    }

}
