package com.ekubrakov.remoteplayer.view_interface;

import com.ekubrakov.remoteplayer.entities.Folder;

/**
 * Created by ekubrakov on 09.08.17.
 */

public interface FolderSelectionView {
    void hideHomeAsUp();
    void updateFolders(Folder[] folders);
    void showNewFolderSelectionConfirmationAlert();
    void openNewPlaylist();
    void returnToPlaylist();
    void showProgressIndicator(boolean needsShowing);
    void showErrorMessage(String text);
}
