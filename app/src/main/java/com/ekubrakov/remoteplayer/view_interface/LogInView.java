package com.ekubrakov.remoteplayer.view_interface;

/**
 * Created by ekubrakov on 08.08.17.
 */

public interface LogInView {
    void openOAuthBrowser();
    void showAuthorizationFailedMessage();
}
