package com.ekubrakov.remoteplayer.view_interface;

/**
 * Created by ekubrakov on 09.08.17.
 */

public interface OAuthBrowserView {
    void openOAuthDialog(String url);
    void openFolderSelection();
    void returnToLogIn();
    void showProgressIndicator(boolean needsShowing);
    void showErrorMessage(String text);
}
