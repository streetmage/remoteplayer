package com.ekubrakov.remoteplayer.database.dao;

import com.ekubrakov.remoteplayer.entities.Song;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekubrakov on 03.07.17.
 */

public final class SongDAO extends BaseDaoImpl<Song, String> {

    public SongDAO(ConnectionSource connectionSource,
                   Class<Song> dataClass) throws java.sql.SQLException {
        super(connectionSource, dataClass);
    }

    public List<Song> getAll() {
        List<Song> songs = new ArrayList();
        try {
            songs = this.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return songs;
        }
    }

    public List<Song> getDownloadedSongs() {
        List<Song> downloadedSongs = new ArrayList<>();
        try {
            downloadedSongs = queryBuilder().where().isNotNull("localPath").query();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return downloadedSongs;
        }
    }

    public void saveSong(Song song) {

        if (song == null) {
            return;
        }

        try {
            createOrUpdate(song);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void saveSongs(List<Song> songs) {

        if (songs == null) {
            return;
        }

        for (Song song : songs) {
            try {
                Song cachedSong = queryForId(song.getSongId());
                if (cachedSong == null) {
                    create(song);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Song> deleteObsolete(List<Song> songs) {

        if (songs == null) {
            return null;
        }

        ArrayList<Song> deletedSongs = new ArrayList<>();

        try {
            List<Song> cachedSongs = queryForAll();
            for (Song cachedSong : cachedSongs) {
                boolean songFound = false;
                for (Song song : songs) {
                    if (song.getSongId().equals(cachedSong.getSongId())) {
                        songFound = true;
                        break;
                    }
                }
                if (songFound == false) {
                    deleteById(cachedSong.getSongId());
                    deletedSongs.add(cachedSong);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return deletedSongs;

    }

}
