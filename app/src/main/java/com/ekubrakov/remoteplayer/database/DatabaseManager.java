package com.ekubrakov.remoteplayer.database;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * Created by ekubrakov on 03.07.17.
 */

public class DatabaseManager {

    private static volatile DatabaseHelper sDatabaseHelper;

    public static DatabaseHelper getDatabaseHelper() {
        return sDatabaseHelper;
    }

    public static void createDatabaseHelper(Context context) {
        sDatabaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
    }

    public static void releaseDatabaseHelper() {
        OpenHelperManager.releaseHelper();
        sDatabaseHelper = null;
    }
}
