package com.ekubrakov.remoteplayer.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.ekubrakov.remoteplayer.database.dao.AccountDAO;
import com.ekubrakov.remoteplayer.database.dao.FolderDAO;
import com.ekubrakov.remoteplayer.database.dao.SongDAO;
import com.ekubrakov.remoteplayer.entities.Account;
import com.ekubrakov.remoteplayer.entities.Folder;
import com.ekubrakov.remoteplayer.entities.Song;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by ekubrakov on 03.07.17.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME ="local_storage.db";
    private static final int DATABASE_VERSION = 1;

    private SongDAO mSongDAO;
    private AccountDAO mAccountDAO;
    private FolderDAO mFolderDAO;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource){
        try {
            TableUtils.createTable(connectionSource, Song.class);
            TableUtils.createTable(connectionSource, Account.class);
            TableUtils.createTable(connectionSource, Folder.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer){
        try{
            TableUtils.dropTable(connectionSource, Song.class, true);
            TableUtils.dropTable(connectionSource, Account.class, true);
            TableUtils.dropTable(connectionSource, Folder.class, true);
            onCreate(db, connectionSource);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public SongDAO getSongDAO() {
        if(mSongDAO == null) {
            try {
                mSongDAO = new SongDAO(getConnectionSource(), Song.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mSongDAO;
    }

    public AccountDAO getAccountDAO() {
        if (mAccountDAO == null) {
            try {
                mAccountDAO = new AccountDAO(getConnectionSource(), Account.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mAccountDAO;
    }

    public FolderDAO getFolderDAO() {
        if (mFolderDAO == null) {
            try {
                mFolderDAO = new FolderDAO(getConnectionSource(), Folder.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mFolderDAO;
    }

    public void clearSongsTable() {
        try {
            TableUtils.clearTable(getConnectionSource(), Song.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close(){
        super.close();
        mSongDAO = null;
        mAccountDAO = null;
        mFolderDAO = null;
    }
}
