package com.ekubrakov.remoteplayer.database.dao;

import android.util.Log;

import com.ekubrakov.remoteplayer.entities.Folder;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by ekubrakov on 04.08.17.
 */

public class FolderDAO extends BaseDaoImpl<Folder, String> {

    public FolderDAO(ConnectionSource connectionSource,
                      Class<Folder> dataClass) throws java.sql.SQLException {
        super(connectionSource, dataClass);
    }

    public void saveFolder(Folder folder) {

        if (folder == null) {
            return;
        }

        try {
            createOrUpdate(folder);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int refresh(Folder folder) {
        int rowsNumberFound = 0;
        try {
            rowsNumberFound = super.refresh(folder);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsNumberFound;
    }

    public int delete(Folder folder) {
        int rowsNumberDeleted = 0;
        try {
            rowsNumberDeleted = super.delete(folder);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsNumberDeleted;
    }

}
