package com.ekubrakov.remoteplayer.database.dao;

import com.ekubrakov.remoteplayer.entities.Account;
import com.ekubrakov.remoteplayer.entities.Folder;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by ekubrakov on 02.08.17.
 */

public class AccountDAO extends BaseDaoImpl<Account, String> {

    public AccountDAO(ConnectionSource connectionSource,
                      Class<Account> dataClass) throws java.sql.SQLException {
        super(connectionSource, dataClass);
    }

    public Account getAccount(String accountId) {
        Account account = null;
        try {
            account = queryForId(accountId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return account;
    }

    public void createOrUpdateAccount(Account account) {

        if (account == null) {
            return;
        }

        try {
            Account cachedAccount = queryForId(account.getAccountId());
            if (cachedAccount == null) {
                create(account);
            } else {
                cachedAccount.setDisplayName(account.getDisplayName());
                cachedAccount.setProfileImagePath(account.getProfileImagePath());
                update(cachedAccount);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Folder getFolderForAccountId(String accountId) {
        Folder folder = null;
        try {
            Account account = queryForId(accountId);
            folder = account.getFolder();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return folder;
    }

    public void updateFolderForAccountId(Folder folder, String accountId) {

        if (accountId == null) {
            return;
        }

        try {
            Account account = queryForId(accountId);
            account.setFolder(folder);
            update(account);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
