package com.ekubrakov.remoteplayer.application;

import android.app.Application;

import com.ekubrakov.remoteplayer.database.DatabaseManager;
import com.ekubrakov.remoteplayer.preferences.ApplicationPreferences;
import com.facebook.stetho.Stetho;

/**
 * Created by ekubrakov on 30.06.17.
 */

public class RemotePlayerApplication extends Application {

    private static RemotePlayerApplication sRemotePlayerApplication;

    public static RemotePlayerApplication getInstance() {
        return sRemotePlayerApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);

        sRemotePlayerApplication = this;

        DatabaseManager.createDatabaseHelper(this);
        ApplicationPreferences.getInstance().setContext(this);
    }

    @Override
    public void onTerminate() {
        sRemotePlayerApplication = null;
        DatabaseManager.releaseDatabaseHelper();
        super.onTerminate();
    }

}
