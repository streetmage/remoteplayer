package com.ekubrakov.remoteplayer.presenter;

import android.support.v4.app.ActivityCompat;

import java.lang.ref.WeakReference;

/**
 * Created by ekubrakov on 08.08.17.
 */

public abstract class BasePresenter<V> {

    protected V mView;

    public void bindView(V view) {
        mView = view;
    }

    public void unbindView() {
        mView = null;
    }
}
