package com.ekubrakov.remoteplayer.presenter;

import android.net.Uri;
import android.os.Build;
import android.webkit.CookieManager;

import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.entities.Account;
import com.ekubrakov.remoteplayer.models.OAuthBrowserModel;
import com.ekubrakov.remoteplayer.notification_center.NotificationCenter;
import com.ekubrakov.remoteplayer.view_interface.OAuthBrowserView;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by ekubrakov on 09.08.17.
 */

public class OAuthBrowserPresenter extends BasePresenter<OAuthBrowserView> {

    private enum State {IDLE, SITE_LOADING_STARTED, ACCOUNT_INFO_LOADING_IN_PROGRESS}

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    private State mState = State.IDLE;

    private String mGeneratedState;

    private OAuthBrowserModel mModel;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public OAuthBrowserPresenter(OAuthBrowserModel model) {
        mModel = model;
    }

    public void onViewSetup() {

        switch (mState) {
            case IDLE:
                cleanCoockies();
                mView.openOAuthDialog(getAuthorizationUrl());
                mState = State.SITE_LOADING_STARTED;
                break;
            case ACCOUNT_INFO_LOADING_IN_PROGRESS:
                mView.showProgressIndicator(true);
                break;
            default:
                break;
        }
    }

    public boolean onShouldOverrideUrlLoading(Uri uri) {

        if (uri == null) {
            return false;
        }

        if (!uri.getScheme().equals("remoteplayer") || !uri.getAuthority().equals("callback")) {
            return false;
        }

        String fragment = uri.getFragment();
        if (fragment == null) {
            return false;
        }

        extractAccessToken(fragment);
        return true;
    }

    public void loadAccountInfo() {
        mState = State.ACCOUNT_INFO_LOADING_IN_PROGRESS;
        mView.showProgressIndicator(true);
        mModel.getAccountInfo(null, new DataService.AccountInfoListener() {
            @Override
            public void onSuccess(Account account) {
                mModel.setAccountId(account.getAccountId());
                mView.showProgressIndicator(false);
                mView.openFolderSelection();
            }

            @Override
            public void onFailure(int responseCode, String errorMessage) {
                mView.showProgressIndicator(false);
                mView.showErrorMessage(errorMessage);
                NotificationCenter.sendReloginNotification();
            }
        });
    }

    public String getGeneratedState() {
        return mGeneratedState;
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private OAuthBrowserPresenter() {
        //
    }

    private void cleanCoockies() {
        CookieManager cookieManager = CookieManager.getInstance();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeAllCookies(null);
        } else {
            CookieManager.getInstance().removeAllCookie();
        }
    }

    private String getAuthorizationUrl() {
        String authorizationUrl = mModel.getAuthorizationUrl();
        mGeneratedState = getRandomState();
        authorizationUrl = String.format("%s&%s=%s", authorizationUrl, "state", mGeneratedState);
        return authorizationUrl;
    }

    private String getRandomState() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }

    private void extractAccessToken(String fragment) {

        final int keyIndex = 0;
        final int valueIndex = 1;

        String accessToken = null;
        String state = null;

        String[] fragmentParts = fragment.split("&");
        for (String parameter : fragmentParts) {
            String[] keyAndValue = parameter.split("=");
            String key = keyAndValue[keyIndex];
            String value = keyAndValue[valueIndex];
            if (key.equals("access_token")) {
                accessToken = value;
            } else if (key.equals("state")) {
                state = value;
            }
        }

        if (state != null && state.equals(mGeneratedState) && accessToken != null) {
            mModel.setAccessToken(accessToken);
            loadAccountInfo();
        } else {
            mView.returnToLogIn();
        }
    }
}
