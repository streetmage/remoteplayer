package com.ekubrakov.remoteplayer.presenter;

import com.ekubrakov.remoteplayer.view_interface.LogInView;

/**
 * Created by ekubrakov on 08.08.17.
 */

public class LogInPresenter extends BasePresenter<LogInView> {

    public void onLogInButtonClick() {
        mView.openOAuthBrowser();
    }

    public void onAuthorizationFailed() {
        mView.showAuthorizationFailedMessage();
    }
}
