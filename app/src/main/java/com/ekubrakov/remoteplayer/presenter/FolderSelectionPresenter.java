package com.ekubrakov.remoteplayer.presenter;

import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.entities.Folder;
import com.ekubrakov.remoteplayer.models.FolderSelectionModel;
import com.ekubrakov.remoteplayer.view_interface.FolderSelectionView;

import java.util.List;

/**
 * Created by ekubrakov on 09.08.17.
 */

public class FolderSelectionPresenter extends BasePresenter<FolderSelectionView> {

    private enum State {IDLE, IN_PROGRESS, LOADING_FINISHED, LOADING_FAILED}

    /// ============================================================================================
    /// Properties Methods
    /// ============================================================================================

    private State mState = State.IDLE;

    private FolderSelectionModel mModel;

    private boolean mNeedsOpenNewPlaylistActivity;

    private Folder[] mFolders;
    private Folder mPreselectedFolder;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public FolderSelectionPresenter(FolderSelectionModel model, boolean needsOpenNewPlaylistActivity) {
        mModel = model;
        mNeedsOpenNewPlaylistActivity = needsOpenNewPlaylistActivity;
    }

    public void onViewSetup() {

        if (mNeedsOpenNewPlaylistActivity) {
            mView.hideHomeAsUp();
        }

        if (mFolders != null) {
            mView.updateFolders(mFolders);
        }

        switch (mState) {
            case IDLE:
                loadData();
                mState = State.IN_PROGRESS;
                break;
            case IN_PROGRESS:
                mView.showProgressIndicator(true);
                break;
            default:
                break;
        }

    }

    public void onForcedDataReload() {
        mState = State.IN_PROGRESS;
        loadData();
    }

    public void onFolderSelected(Folder folder) {
        mPreselectedFolder = folder;
        mView.showNewFolderSelectionConfirmationAlert();
    }

    public void onNewFolderSelectionConfirmed() {
        mModel.setFolderForAccount(mPreselectedFolder);
        if (mNeedsOpenNewPlaylistActivity) {
            mView.openNewPlaylist();
        } else {
            mView.returnToPlaylist();
        }
    }

    public void onNewFolderSelectionCancelled() {
        mPreselectedFolder = null;
    }

    public void loadData() {
        mView.showProgressIndicator(true);
        mModel.getFolders(new DataService.GetFoldersListener() {
            @Override
            public void onSuccess(List<Folder> folders) {
                mFolders = folders.toArray(new Folder[folders.size()]);
                mView.updateFolders(mFolders);
                mView.showProgressIndicator(false);
                mState = State.LOADING_FINISHED;
            }

            @Override
            public void onFailure(int responseCode, String errorMessage) {
                mView.showErrorMessage(errorMessage);
                mView.showProgressIndicator(false);
                mState = State.LOADING_FAILED;
            }
        });
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private FolderSelectionPresenter() {
        //
    }
}
