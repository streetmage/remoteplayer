package com.ekubrakov.remoteplayer.entities;

import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.resources.Item;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekubrakov on 03.08.17.
 */

@DatabaseTable (tableName = "folders")
public class Folder {

    @DatabaseField(id = true)
    private String folderId;
    @DatabaseField
    private String path;
    @DatabaseField
    private String name;

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<Folder> convertFolders(List<Item> items) {

        if (items == null) {
            return null;
        }

        ArrayList<Folder> folders = new ArrayList<Folder>();
        for (Item item : items) {

            if (item.getType().equals("dir")) {

                if (item.getResourceId() == null) {
                    continue;
                }

                Folder folder = new Folder();
                folder.setFolderId(item.getResourceId());
                folder.setName(item.getName());
                folder.setPath(item.getPath());
                folders.add(folder);

            }

        }

        return folders;
    }

}
