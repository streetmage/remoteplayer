package com.ekubrakov.remoteplayer.entities;

import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.login_info.LoginInfoResponse;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by ekubrakov on 01.08.17.
 */

@DatabaseTable(tableName = "accounts")
public class Account {

    private static String PROFILE_IMAGE_PATH_FORMAT = "https://avatars.yandex.net/get-yapic/%s/islands-200";

    @DatabaseField(id = true)
    private String accountId;
    @DatabaseField
    private String displayName;
    @DatabaseField
    private String profileImagePath;

    @DatabaseField(foreign = true)
    private Folder folder;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfileImagePath() {
        return profileImagePath;
    }

    public void setProfileImagePath(String profileImagePath) {
        this.profileImagePath = profileImagePath;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    public static Account convertResponse(LoginInfoResponse response) {

        if (response == null || response.getId() == null) {
            return null;
        }

        Account account = new Account();
        account.setAccountId(response.getId());
        account.setDisplayName(response.getDisplayName());

        String avatarId = response.getDefaultAvatarId();
        if (avatarId != null) {
            account.setProfileImagePath(String.format(PROFILE_IMAGE_PATH_FORMAT, avatarId));
        }

        return account;
    }
}
