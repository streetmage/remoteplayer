package com.ekubrakov.remoteplayer.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.resources.Item;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekubrakov on 29.06.17.
 */

@DatabaseTable(tableName = "songs")
public class Song implements Parcelable {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    @DatabaseField(id = true)
    private String songId;
    @DatabaseField
    private String name;
    @DatabaseField
    private String remotePath;
    @DatabaseField
    private String downloadPath;
    @DatabaseField
    private String localPath;
    @DatabaseField
    private Integer size;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public Song() {
        super();
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    /// ============================================================================================
    /// Parcelable Methods
    /// ============================================================================================

    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(songId);
        parcel.writeString(name);
        parcel.writeString(remotePath);
        parcel.writeString(downloadPath);
        parcel.writeString(localPath);
        parcel.writeInt(size);
    }

    private Song(Parcel parcel) {
        songId = parcel.readString();
        name = parcel.readString();
        remotePath = parcel.readString();
        downloadPath = parcel.readString();
        localPath = parcel.readString();
        size = parcel.readInt();
    }

    public static List<Song> convertItems(List<Item> items) {

        if (items == null) {
            return null;
        }

        ArrayList<Song> songs = new ArrayList<Song>();
        for (Item item : items) {
            if (item.getMediaType() != null && item.getMediaType().equals("audio")) {

                if (item.getResourceId() == null) {
                    continue;
                }

                Song song = new Song();
                song.setSongId(item.getResourceId());
                song.setName(item.getName());
                song.setRemotePath(item.getPath());
                song.setSize(item.getSize());
                songs.add(song);
            }
        }

        return songs;
    }

}
