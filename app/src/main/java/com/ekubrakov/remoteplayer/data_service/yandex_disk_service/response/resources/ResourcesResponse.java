package com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.resources;

import com.google.gson.annotations.SerializedName;

public class ResourcesResponse {

    @SerializedName("_embedded")
    private Embedded embedded;

    public Embedded getEmbedded() {
        return embedded;
    }

    public void setEmbedded(Embedded embedded) {
        this.embedded = embedded;
    }

}