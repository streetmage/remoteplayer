package com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.resources_download;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ekubrakov on 30.06.17.
 */

public class ResourceDownloadResponse {

    @SerializedName("href")
    private String downloadPath;

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }
}
