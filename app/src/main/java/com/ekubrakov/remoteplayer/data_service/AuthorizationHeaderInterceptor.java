package com.ekubrakov.remoteplayer.data_service;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ekubrakov on 29.06.17.
 */

public class AuthorizationHeaderInterceptor implements Interceptor {

    // region Properties

    private String mAccessToken;

    // endregion

    // region Methods

    public void setAccessToken(String accessToken) {
        this.mAccessToken = accessToken;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();

        if (mAccessToken != null) {
            request = request.newBuilder()
                    .addHeader("Authorization", String.format("OAuth %s", mAccessToken))
                    .build();
        }

        Log.d(this.getClass().toString(), request.toString());

        Response response = chain.proceed(request);

        return response;
    }

    // endregion
}
