package com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.login_info;

import com.google.gson.annotations.SerializedName;

public class LoginInfoResponse {

    @SerializedName("display_name")
    private String displayName;
    @SerializedName("default_avatar_id")
    private String defaultAvatarId;
    @SerializedName("id")
    private String id;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDefaultAvatarId() {
        return defaultAvatarId;
    }

    public void setDefaultAvatarId(String defaultAvatarId) {
        this.defaultAvatarId = defaultAvatarId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}