package com.ekubrakov.remoteplayer.data_service.yandex_disk_service;

import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.login_info.LoginInfoResponse;
import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.resources.ResourcesResponse;
import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.resources_download.ResourceDownloadResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by ekubrakov on 29.06.17.
 */

public interface YandexDiskService {

    @GET("/v1/disk/resources/")
    Call<ResourcesResponse> getResources(@Query("path") String musicCatalogPath,
                                         @Query("limit") int limit);

    @GET("v1/disk/resources/download")
    Call<ResourceDownloadResponse> getDownloadResource(@Query("path") String path);

    @GET
    Call<ResponseBody> downloadSong(@Url String downloadUrl);

    @GET("https://login.yandex.ru/info?format=json")
    Call<LoginInfoResponse> getAccountInfo();
}
