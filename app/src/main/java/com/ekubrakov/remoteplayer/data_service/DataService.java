package com.ekubrakov.remoteplayer.data_service;

import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.ekubrakov.remoteplayer.application.RemotePlayerApplication;
import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.login_info.LoginInfoResponse;
import com.ekubrakov.remoteplayer.database.DatabaseHelper;
import com.ekubrakov.remoteplayer.database.DatabaseManager;
import com.ekubrakov.remoteplayer.database.dao.AccountDAO;
import com.ekubrakov.remoteplayer.database.dao.FolderDAO;
import com.ekubrakov.remoteplayer.entities.Account;
import com.ekubrakov.remoteplayer.entities.Folder;
import com.ekubrakov.remoteplayer.entities.Song;
import com.ekubrakov.remoteplayer.models.FolderSelectionModel;
import com.ekubrakov.remoteplayer.models.OAuthBrowserModel;
import com.ekubrakov.remoteplayer.notification_center.NotificationCenter;
import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.resources.Item;
import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.resources.ResourcesResponse;
import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.YandexDiskService;
import com.ekubrakov.remoteplayer.data_service.yandex_disk_service.response.resources_download.ResourceDownloadResponse;
import com.ekubrakov.remoteplayer.preferences.ApplicationPreferences;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ekubrakov on 29.06.17.
 */

public class DataService implements OAuthBrowserModel, FolderSelectionModel {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    private static final int UNDEFINED_ERROR_CODE = -999;

    private static final int DEFAULT_PAGE_LIMIT = 1000;

    private static DataService sDataService;

    private AuthorizationHeaderInterceptor mHeaderInterceptor;

    private YandexDiskService mYandexDiskService;
    private DatabaseHelper mDatabaseHelper;
    private ApplicationPreferences mPreferences;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public static DataService getInstance() {
        if (sDataService == null) {
            sDataService = new DataService();
        }
        return sDataService;
    }

    public void setAccessToken(String accessToken) {
        mPreferences.setAccessToken(accessToken);
        mHeaderInterceptor.setAccessToken(accessToken);
    }

    public String getAccessToken() {
        return mPreferences.getAccessToken();
    }

    public void removeAccessToken() {
        mPreferences.removeAccessToken();
    }

    public String getAccountId() {
        return mPreferences.getAccountId();
    }

    public void setAccountId(String accountId) {
        mPreferences.setAccountId(accountId);
    }

    public boolean isLoggedIn() {
        return mPreferences.isLoggedIn();
    }


    public String getAuthorizationUrl() {
        Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.scheme("https");
        uriBuilder.authority("oauth.yandex.ru");
        uriBuilder.appendPath("authorize");
        uriBuilder.appendQueryParameter("response_type", "token");
        uriBuilder.appendQueryParameter("client_id", getClientId());
        uriBuilder.appendQueryParameter("device_id", getDeviceId());
        uriBuilder.appendQueryParameter("device_name", getDeviceName());
        uriBuilder.appendQueryParameter("force_confirm", "yes");
        return uriBuilder.build().toString();
    }

    private String getClientId() {
        return "65070db4f2394b588195b54c6f924e52";
    }

    private String getDeviceId() {
        return Settings.Secure.getString(RemotePlayerApplication.getInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private String getDeviceName() {
        return Build.MODEL;
    }

    public void getFolders(final GetFoldersListener listener) {
        mYandexDiskService.getResources("disk:/", DEFAULT_PAGE_LIMIT).enqueue(new Callback<ResourcesResponse>() {
            @Override
            public void onResponse(Call<ResourcesResponse> call, Response<ResourcesResponse> response) {
                if (response.isSuccessful()) {
                    List<Item> items = response.body().getEmbedded().getItems();
                    List<Folder> folders = Folder.convertFolders(items);
                    listener.onSuccess(folders);
                } else {
                    handleFailureResponse(response.code(), response.message(), listener);
                }
            }

            @Override
            public void onFailure(Call<ResourcesResponse> call, Throwable t) {
                handleFailureResponse(UNDEFINED_ERROR_CODE, t.getLocalizedMessage(), listener);
            }
        });
    }

    public void getSongs(String folderPath, final GetSongsListener listener) {

        List<Song> cachedSongs = mDatabaseHelper.getSongDAO().getAll();
        if (cachedSongs != null) {
            listener.onSuccess(cachedSongs);
        }

        mYandexDiskService.getResources(folderPath, DEFAULT_PAGE_LIMIT).enqueue(new Callback<ResourcesResponse>() {
            @Override
            public void onResponse(Call<ResourcesResponse> call, retrofit2.Response<ResourcesResponse> response) {
                if (response.isSuccessful()) {
                    List<Item> items = response.body().getEmbedded().getItems();
                    List<Song> songs = Song.convertItems(items);
                    List<Song> deletedSongs = mDatabaseHelper.getSongDAO().deleteObsolete(songs);
                    deleteSongFiles(deletedSongs);
                    mDatabaseHelper.getSongDAO().saveSongs(songs);
                    songs = mDatabaseHelper.getSongDAO().getAll();
                    listener.onSuccess(songs);
                } else {
                    handleFailureResponse(response.code(), response.message(), listener);
                }
            }

            @Override
            public void onFailure(Call<ResourcesResponse> call, Throwable t) {
                handleFailureResponse(UNDEFINED_ERROR_CODE, t.getLocalizedMessage(), listener);
            }
        });
    }

    public void getDownloadPathForSong(final Song song, final GetDownloadResourceUrlListener listener) {

        mYandexDiskService.getDownloadResource(song.getRemotePath()).enqueue(new Callback<ResourceDownloadResponse>() {
            @Override
            public void onResponse(Call<ResourceDownloadResponse> call, retrofit2.Response<ResourceDownloadResponse> response) {
                if (response.isSuccessful()) {
                    song.setDownloadPath(response.body().getDownloadPath());
                    mDatabaseHelper.getSongDAO().saveSong(song);
                    listener.onSuccess(song);
                } else {
                    handleFailureResponse(response.code(), response.message(), listener);
                }
            }

            @Override
            public void onFailure(Call<ResourceDownloadResponse> call, Throwable t) {
                handleFailureResponse(UNDEFINED_ERROR_CODE, t.getLocalizedMessage(), listener);
            }
        });

    }

    public void downloadSong(final Song song, final String directoryPath, final SongDownloadListener listener) {

        mYandexDiskService.downloadSong(song.getDownloadPath()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Uri uri = Uri.parse(song.getDownloadPath());
                    String storeFilePath = String.format("%s%s", directoryPath, uri.getLastPathSegment());
                    boolean writtenToDisk = writeResponseBodyToDisk(response.body(), storeFilePath);
                    if (writtenToDisk) {
                        song.setLocalPath(storeFilePath);
                        listener.onSuccess(song);
                    } else {
                        listener.onFailure(UNDEFINED_ERROR_CODE, "File hasn't been saved");
                    }
                } else {
                    listener.onFailure(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onFailure(UNDEFINED_ERROR_CODE, t.getLocalizedMessage());
            }
        });
    }

    public void getAccountInfo(@Nullable String accountId, final AccountInfoListener listener) {

        if (accountId != null) {
            Account account = mDatabaseHelper.getAccountDAO().getAccount(accountId);
            mDatabaseHelper.getFolderDAO().refresh(account.getFolder());
            if (account != null) {
                listener.onSuccess(account);
            }
        }

        mYandexDiskService.getAccountInfo().enqueue(new Callback<LoginInfoResponse>() {
            @Override
            public void onResponse(Call<LoginInfoResponse> call, Response<LoginInfoResponse> response) {
                if (response.isSuccessful()) {
                    Account account = Account.convertResponse(response.body());
                    if (account != null) {
                        mDatabaseHelper.getAccountDAO().createOrUpdateAccount(account);
                        listener.onSuccess(account);
                    } else {
                        listener.onFailure(UNDEFINED_ERROR_CODE, "Mapping failed");
                    }
                } else {
                    listener.onFailure(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<LoginInfoResponse> call, Throwable t) {
                listener.onFailure(UNDEFINED_ERROR_CODE, t.getLocalizedMessage());
            }
        });
    }

    public Folder getFolderForAccount() {
        Account account = mDatabaseHelper.getAccountDAO().getAccount(getAccountId());
        mDatabaseHelper.getFolderDAO().refresh(account.getFolder());
        return account.getFolder();
    }

    public void setFolderForAccount(Folder folder) {

        AccountDAO accountDAO = mDatabaseHelper.getAccountDAO();
        FolderDAO folderDAO = mDatabaseHelper.getFolderDAO();

        Folder folderToDelete = accountDAO.getFolderForAccountId(mPreferences.getAccountId());
        folderDAO.delete(folderToDelete);
        folderDAO.saveFolder(folder);
        accountDAO.updateFolderForAccountId(folder, mPreferences.getAccountId());
    }

    public void deleteAllSongs() {
        List<Song> downloadedSongs = mDatabaseHelper.getSongDAO().getDownloadedSongs();
        deleteSongFiles(downloadedSongs);
        mDatabaseHelper.clearSongsTable();
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private DataService() {

        mPreferences = ApplicationPreferences.getInstance();

        mHeaderInterceptor = new AuthorizationHeaderInterceptor();
        mHeaderInterceptor.setAccessToken(mPreferences.getAccessToken());

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        StethoInterceptor stethoInterceptor = new StethoInterceptor();

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(mHeaderInterceptor)
                .addInterceptor(loggingInterceptor)
                .addNetworkInterceptor(stethoInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://cloud-api.yandex.net:443/")
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mYandexDiskService = retrofit.create(YandexDiskService.class);

        mDatabaseHelper = DatabaseManager.getDatabaseHelper();
    }

    private void handleFailureResponse(int responseCode,
                                       String errorMessage,
                                       FailureListener failureListener) {
        if (responseCode == 401) {
            NotificationCenter.sendReloginNotification();
        } else {
            failureListener.onFailure(UNDEFINED_ERROR_CODE, errorMessage);
        }
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, String filePath) {
        try {

            File audioFile = new File(filePath);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(audioFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    private void deleteSongFiles(List<Song> deletedSongs) {
        for (Song song : deletedSongs) {
            String localPath = song.getLocalPath();
            if (localPath != null) {
                File songFile = new File(localPath);
                songFile.delete();
            }
        }
    }

    /// ============================================================================================
    /// Listeners
    /// ============================================================================================

    public interface FailureListener {
        void onFailure(int responseCode, String errorMessage);
    }

    public interface GetFoldersListener extends FailureListener {
        void onSuccess(List<Folder> folders);
    }

    public interface GetSongsListener extends FailureListener {
        void onSuccess(List<Song> songs);
    }

    public interface GetDownloadResourceUrlListener extends FailureListener {
        void onSuccess(Song song);
    }

    public interface SongDownloadListener extends FailureListener {
        void onSuccess(Song song);
    }

    public interface AccountInfoListener extends FailureListener {
        void onSuccess(Account account);
    }
}
