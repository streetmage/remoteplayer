package com.ekubrakov.remoteplayer.worker_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.ekubrakov.remoteplayer.models.DrawerModel;

/**
 * Created by ekubrakov on 02.08.17.
 */

public class DrawerWorkerFragment extends Fragment {

    private DrawerModel mModel = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public DrawerModel getModel() {
        if (mModel == null) {
            mModel = new DrawerModel();
        }
        return mModel;
    }
}
