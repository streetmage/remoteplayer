package com.ekubrakov.remoteplayer.worker_fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ekubrakov.remoteplayer.models.PlayListModel;

/**
 * Created by ekubrakov on 29.06.17.
 */

public class PlayListWorkerFragment extends Fragment {

    // region Properties

    private PlayListModel mModel;

    // endregion

    // region Method

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public PlayListModel getModel() {
        if (mModel == null) {
            mModel = new PlayListModel(getActivity().getApplicationContext());
        }
        return mModel;
    }

    // endregion

}
