package com.ekubrakov.remoteplayer.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.RemoteViews;

import com.ekubrakov.remoteplayer.R;
import com.ekubrakov.remoteplayer.activity.PlayListActivity;
import com.ekubrakov.remoteplayer.entities.Song;
import com.ekubrakov.remoteplayer.models.MusicPlayerModel;

import java.util.List;

/**
 * Created by ekubrakov on 24.07.17.
 */

public class MusicPlayerService extends Service implements MusicPlayerModel.Listener {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    private static final int MUSIC_PLAYER_NOTIFICATION_ID = 999;
    private static final int PENDING_INTENT_REQUEST_CODE = 0;

    public static final String SONGS_EXTRA = "SONGS_EXTRA";
    public static final String SONG_TO_PLAY_POSITION_EXTRA = "SONG_TO_PLAY_POSITION_EXTRA";

    public static final String PLAY_SONGS_SERVICE_ACTION = "PLAY_SONGS_SERVICE_ACTION";
    public static final String PAUSE_PLAYBACK_ACTION = "PAUSE_PLAYBACK_ACTION";
    public static final String RESUME_PLAYBACK_ACTION = "RESUME_PLAYBACK_ACTION";
    public static final String PLAY_PREVIOUS_SONG_ACTION = "PLAY_PREVIOUS_SONG_ACTION";
    public static final String PLAY_NEXT_SONG_ACTION = "PLAY_NEXT_SONG_ACTION";
    public static final String STOP_SERVICE_ACTION = "STOP_SERVICE_ACTION";
    public static final String OPEN_PLAY_LIST_ACTIVITY_ACTION = "OPEN_PLAY_LIST_ACTIVITY_ACTION";

    private static boolean sServiceRunning;

    private MusicPlayerModel mModel;

    private RemoteViews mNotificationViews;
    private Notification mForegroundNotification;

    private MusicPlayerServiceBinder mBinder;

    private DownloaderProgressReceiver mDownloaderProgressReceiver = new DownloaderProgressReceiver();
    private HeadsetStateBroadcastReceiver mHeadsetStateBroadcastReceiver = new HeadsetStateBroadcastReceiver();

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (mModel == null) {
            mModel = new MusicPlayerModel(this, this);
        }

        sServiceRunning = true;

        setupDownloaderProgressReceiver();
        setupHeadsetStateBroadcastReceiver();

        handleReceivedIntent(intent);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if (mBinder == null) {
            mBinder = new MusicPlayerServiceBinder();
        }
        return mBinder;
    }

    @Override
    public void onDestroy() {
        sServiceRunning = false;
        super.onDestroy();
    }

    public static boolean isServiceRunning() {
        return sServiceRunning;
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private void handleReceivedIntent(Intent intent) {

        if (intent == null) {
            return;
        }

        String action = intent.getAction();

        if (action.equals(PLAY_SONGS_SERVICE_ACTION)) {
            startPlayback(intent);
        } else if (action.equals(PAUSE_PLAYBACK_ACTION)) {
            pausePlayback();
        } else if (action.equals(RESUME_PLAYBACK_ACTION)) {
            resumePlayback();
        } else if (action.equals(PLAY_PREVIOUS_SONG_ACTION)) {
            playPreviousSong();
        } else if (action.equals(PLAY_NEXT_SONG_ACTION)) {
            playNextSong();
        } else if (action.equals(OPEN_PLAY_LIST_ACTIVITY_ACTION)) {
            openPlayListActivity();
        }

        if (action.equals(STOP_SERVICE_ACTION)) {
            stopPlayback();
        } else {
            buildForegroundNotification();
            updateForegroundNotification();
        }

    }

    private void buildForegroundNotification() {

        if (mForegroundNotification != null) {
            return;
        }

        mNotificationViews = new RemoteViews(getPackageName(), R.layout.notification_music_player);

        Intent startPlaylistActivityIntent = new Intent(this, PlayListActivity.class);
        PendingIntent startPlaylistActivityPendingIntent = PendingIntent.getActivity(this,
                0, startPlaylistActivityIntent, PendingIntent.FLAG_NO_CREATE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getBaseContext());
        builder.setContentTitle(this.getClass().toString());
        builder.setContentText(this.getClass().toString());
        builder.setTicker(this.getClass().toString());
        builder.setSmallIcon(R.drawable.ic_notification_small_icon);
        builder.setCustomContentView(mNotificationViews);
        builder.setOngoing(true);
        builder.setVisibility(Notification.VISIBILITY_PUBLIC);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        builder.setContentIntent(startPlaylistActivityPendingIntent);
        mForegroundNotification = builder.build();
    }

    private void updateForegroundNotification() {

        // Configure custom UI

        Song currentPlayingSong = mModel.getCurrentPlayingSong();
        String currentPlayingSongName =  currentPlayingSong != null ? currentPlayingSong.getName() : getResources().getString(R.string.playback_error);
        mNotificationViews.setTextViewText(R.id.notification_song_name, currentPlayingSongName);

        int playOrPauseResource = mModel.isPlaybackInProgress() ? R.drawable.ic_player_pause : R.drawable.ic_player_play;
        mNotificationViews.setImageViewResource(R.id.notification_play_or_pause_song_button, playOrPauseResource);

        final int requestCode = PENDING_INTENT_REQUEST_CODE;
        final int flag = PendingIntent.FLAG_CANCEL_CURRENT;

        // Setup buttons handlers

        Intent openPlayListActivityAction = new Intent(this, MusicPlayerService.class);
        openPlayListActivityAction.setAction(OPEN_PLAY_LIST_ACTIVITY_ACTION);
        PendingIntent openPlayListActivityPendingIntent = PendingIntent.getService(this, requestCode, openPlayListActivityAction, flag);
        mNotificationViews.setOnClickPendingIntent(R.id.button_open_playlist, openPlayListActivityPendingIntent);

        if (mModel.isPlaybackInProgress()) {
            Intent pauseSongIntent = new Intent(this, MusicPlayerService.class);
            pauseSongIntent.setAction(PAUSE_PLAYBACK_ACTION);
            PendingIntent pausePendingIntent = PendingIntent.getService(this, requestCode, pauseSongIntent, flag);
            mNotificationViews.setOnClickPendingIntent(R.id.notification_play_or_pause_song_button, pausePendingIntent);
        } else {
            Intent playSongIntent = new Intent(this, MusicPlayerService.class);
            playSongIntent.setAction(RESUME_PLAYBACK_ACTION);
            PendingIntent playPendingIntent = PendingIntent.getService(this, requestCode, playSongIntent, flag);
            mNotificationViews.setOnClickPendingIntent(R.id.notification_play_or_pause_song_button, playPendingIntent);
        }

        Intent previousSongIntent = new Intent(this, MusicPlayerService.class);
        previousSongIntent.setAction(PLAY_PREVIOUS_SONG_ACTION);
        PendingIntent previousSongPendingIntent = PendingIntent.getService(this, requestCode, previousSongIntent, flag);
        mNotificationViews.setOnClickPendingIntent(R.id.notification_previous_song_button, previousSongPendingIntent);

        Intent nextSongIntent = new Intent(this, MusicPlayerService.class);
        nextSongIntent.setAction(PLAY_NEXT_SONG_ACTION);
        PendingIntent nextSongPendingIntent = PendingIntent.getService(this, requestCode, nextSongIntent, flag);
        mNotificationViews.setOnClickPendingIntent(R.id.notification_next_song_button, nextSongPendingIntent);

        Intent stopServiceIntent = new Intent(this, MusicPlayerService.class);
        stopServiceIntent.setAction(STOP_SERVICE_ACTION);
        PendingIntent stopServicePendingIntent = PendingIntent.getService(this, requestCode, stopServiceIntent, flag);
        mNotificationViews.setOnClickPendingIntent(R.id.notification_close_button, stopServicePendingIntent);

        startForeground(MUSIC_PLAYER_NOTIFICATION_ID, mForegroundNotification);
    }

    private void stopPlayback() {
        mModel.stopPlaying();
        notifyPausedPlayback();
        stopSelf();
    }

    private void startPlayback(Intent intent) {
        List<Song> songs = intent.getParcelableArrayListExtra(SONGS_EXTRA);
        mModel.setSongs(songs);
        int position = intent.getIntExtra(SONG_TO_PLAY_POSITION_EXTRA, 0);
        mModel.playSong(position);
    }

    private void pausePlayback() {
        mModel.pausePlayback();
        updateForegroundNotification();
        notifyPausedPlayback();
    }

    private void resumePlayback() {
        mModel.resumePlayback();
        updateForegroundNotification();
    }

    private void playPreviousSong() {
        mModel.playPreviousSong();
        updateForegroundNotification();
    }

    private void playNextSong() {
        mModel.playNextSong();
        updateForegroundNotification();
    }

    private void openPlayListActivity() {
        Intent intent = new Intent(this, PlayListActivity.class);
        startActivity(intent);
        sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
    }

    private void notifyPausedPlayback() {
        Intent intent = new Intent(this, PlayListActivity.class);
        intent.setAction(PlayListActivity.SHOW_RESUME_PLAYBACK);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void notifyAboutCurrentSongPlaying() {
        Intent intent = new Intent(this, PlayListActivity.class);
        intent.setAction(PlayListActivity.SHOW_SONG_PLAYBACK);
        intent.putExtra(PlayListActivity.SONG_PLAYING_EXTRA, mModel.getCurrentPlayingSong());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /// ============================================================================================
    /// MusicPlayerModel.Listener Methods
    /// ============================================================================================

    @Override
    public void onMusicPlayerModelChangedSong(MusicPlayerModel model) {

        notifyAboutCurrentSongPlaying();

        if (mForegroundNotification != null) {
            updateForegroundNotification();
        }
    }

    /// ============================================================================================
    /// MusicPlayerServiceBinder Class
    /// ============================================================================================

    public class MusicPlayerServiceBinder extends Binder {

        public Song getCurrentPlayingSong() {
            return mModel.getCurrentPlayingSong();
        }

        public boolean isPlaybackInProgress() {
            return mModel.isPlaybackInProgress();
        }

        public void pausePlayback() {
            MusicPlayerService.this.pausePlayback();
        }

        public void resumePlayback() {
            MusicPlayerService.this.resumePlayback();
        }

        public void playPreviousSong() {
            MusicPlayerService.this.playPreviousSong();
        }

        public void playNextSong() {
            MusicPlayerService.this.playNextSong();
        }

        public double getCurrentPlayingSongProgress() {
            return mModel.getCurrentPlayingSongProgress();
        }

        public void setCurrentPlayingSongProgress(double progress) {
            mModel.setCurrentPlayingSongProgress(progress);
        }
    }

    /// ============================================================================================
    /// DownloaderProgressReceiver Class
    /// ============================================================================================

    private class DownloaderProgressReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleDownloaderProgressReceiverIntent(intent);
        }
    }

    private void setupDownloaderProgressReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DownloadService.UPDATE_SONG_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mDownloaderProgressReceiver, intentFilter);
    }

    private void handleDownloaderProgressReceiverIntent(Intent intent) {

        String action = intent.getAction();

        if (action.equals(DownloadService.UPDATE_SONG_ACTION)) {
            Song song = intent.getParcelableExtra(DownloadService.SONG_EXTRA_KEY);
            mModel.updateSong(song);
        }
    }

    /// ============================================================================================
    /// HeadsetStateBroadcastReceiver Class
    /// ============================================================================================

    private class HeadsetStateBroadcastReceiver extends BroadcastReceiver {

        private final String STATE_EXTRA = "state";

        @Override
        public void onReceive(final Context context, final Intent intent) {
            // Wired headset monitoring
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra(STATE_EXTRA, 0);
                // If headset unplugged
                if (state == 0 && mModel.isPlaybackInProgress()) {
                    pausePlayback();
                }
            }
        }
    }

    private void setupHeadsetStateBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(mHeadsetStateBroadcastReceiver, intentFilter);
    }

}
