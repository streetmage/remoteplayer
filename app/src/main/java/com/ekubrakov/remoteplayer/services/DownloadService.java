package com.ekubrakov.remoteplayer.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.ekubrakov.remoteplayer.R;
import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.database.DatabaseManager;
import com.ekubrakov.remoteplayer.entities.Song;

/**
 * Created by ekubrakov on 03.07.17.
 */

public class DownloadService extends IntentService {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    public static final String SONG_EXTRA_KEY = "song";

    public static final String DOWNLOAD_SONG_ACTION = "DOWNLOAD_SONG_ACTION";
    public static final String STOP_SERVICE_ACTION = "STOP_SERVICE_ACTION";

    public static final String UPDATE_SONG_ACTION = "UPDATE_SONG_ACTION";
    public static final String PERFORM_AFTER_DOWNLOAD_ACTION = "PERFORM_AFTER_DOWNLOAD_ACTION";

    private DataService mDataService = DataService.getInstance();

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public DownloadService() {
        super(DownloadService.class.toString());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if (intent.getAction().equals(DOWNLOAD_SONG_ACTION)) {

            Song song = intent.getParcelableExtra(SONG_EXTRA_KEY);
            downloadSong(song);

        } else if (intent.getAction().equals(STOP_SERVICE_ACTION)) {
            stopSelf();
        }
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private void downloadSong(final Song song) {

        mDataService.getDownloadPathForSong(song, new DataService.GetDownloadResourceUrlListener() {
            @Override
            public void onSuccess(final Song song) {
                mDataService.downloadSong(song, getFilesDir().getAbsolutePath(), new DataService.SongDownloadListener() {
                    @Override
                    public void onSuccess(Song song) {
                        notifySongUpdated(song);
                    }

                    @Override
                    public void onFailure(int responseCode, String errorMessage) {
                        notifySongLoadingFailed(errorMessage, song);
                    }
                });
            }

            @Override
            public void onFailure(int responseCode, String errorMessage) {
                notifySongLoadingFailed(errorMessage, song);
            }
        });
    }

    private void notifySongUpdated(Song song) {

        String toastText = String.format(this.getResources().getString(R.string.file_downloaded), song.getName());
        Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();

        DatabaseManager.getDatabaseHelper().getSongDAO().saveSong(song);
        Intent intent = new Intent(PERFORM_AFTER_DOWNLOAD_ACTION);
        intent.putExtra(SONG_EXTRA_KEY, song);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void notifySongLoadingFailed(String errorMessage, Song song) {
        Toast.makeText(DownloadService.this, errorMessage, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(PERFORM_AFTER_DOWNLOAD_ACTION);
        intent.putExtra(SONG_EXTRA_KEY, song);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
