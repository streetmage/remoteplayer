package com.ekubrakov.remoteplayer.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ekubrakov.remoteplayer.R;
import com.ekubrakov.remoteplayer.activity.FolderSelectionActivity;
import com.ekubrakov.remoteplayer.entities.Account;
import com.ekubrakov.remoteplayer.models.DrawerModel;
import com.ekubrakov.remoteplayer.notification_center.NotificationCenter;
import com.ekubrakov.remoteplayer.preferences.ApplicationPreferences;
import com.ekubrakov.remoteplayer.worker_fragments.DrawerWorkerFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ekubrakov.remoteplayer.activity.FolderSelectionActivity.SELECT_FOLDER_REQUEST_CODE;

/**
 * Created by ekubrakov on 01.08.17.
 */

public class DrawerFragment extends Fragment implements DrawerModel.Observer {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    private static final String WORKER_FRAGMENT_TAG = "WORKER_FRAGMENT_TAG";

    @BindView(R.id.account_image_view) ImageView mAccountImageView;
    @BindView(R.id.full_name_text_view) TextView mFullNameTextView;
    @BindView(R.id.choose_folder_button) Button mSelectFolderButton;
    @BindView(R.id.version_text_view) TextView mVersionTextView;
    @BindView(R.id.sail_away_button) ImageButton mLogoutButton;

    private DrawerModel mModel;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drawer, container, false);
        setupView(view);
        setupWorkerFragment();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mModel.unregisterObserver(this);
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private void setupView(View view) {
        ButterKnife.bind(this, view);
        mSelectFolderButton.setOnClickListener(v -> selectFolder());
        mLogoutButton.setOnClickListener(v -> logout());
    }

    private void setupWorkerFragment() {

        DrawerWorkerFragment workerFragment = (DrawerWorkerFragment) getFragmentManager().findFragmentByTag(WORKER_FRAGMENT_TAG);
        if (workerFragment == null) {
            workerFragment = new DrawerWorkerFragment();
            getFragmentManager().beginTransaction()
                    .add(workerFragment, WORKER_FRAGMENT_TAG)
                    .commit();
        }

        mModel = workerFragment.getModel();
        mModel.registerObserver(this);
        mModel.setContext(this.getContext());
        mModel.loadAccountInfo();
        updateAccountInfo();
        updateVersion();

    }

    private void selectFolder() {
        Intent intent = new Intent(getContext(), FolderSelectionActivity.class);
        getActivity().startActivityForResult(intent, SELECT_FOLDER_REQUEST_CODE);
    }

    private void logout() {
        NotificationCenter.sendReloginNotification();
    }

    private void updateVersion() {
        mVersionTextView.setText(String.format("%s %s (%d)",
                getResources().getString(R.string.version), mModel.getVersionName(), mModel.getVersionCode()));
    }

    private void updateAccountInfo() {
        Account account = mModel.getAccount();
        if (account != null) {
            mFullNameTextView.setText(account.getDisplayName());
            Glide.with(this).load(account.getProfileImagePath()).apply(RequestOptions.circleCropTransform()).into(mAccountImageView);
        }
    }

    /// ============================================================================================
    /// DrawerModel.Observer Methods
    /// ============================================================================================


    @Override
    public void onAccountInfoLoaded(DrawerModel model) {
        updateAccountInfo();
    }

    @Override
    public void onAccountInfoLoadingFailed(DrawerModel model, String errorMessage) {
        Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG).show();
    }
}
