package com.ekubrakov.remoteplayer.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ekubrakov.remoteplayer.R;
import com.ekubrakov.remoteplayer.entities.Folder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ekubrakov on 03.08.17.
 */

public class FoldersGridAdapter extends RecyclerView.Adapter <FoldersGridAdapter.ViewHolder> {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    private FoldersGridAdapterListener mListener;
    private Folder[] mFolders;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public FoldersGridAdapter(FoldersGridAdapterListener listener) {
        mListener = listener;
    }

    public void setFolders(Folder[] folders) {
        mFolders = folders;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mFolders != null ? mFolders.length : 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_folder, parent, false);
        return new ViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindFolder(mFolders[position]);
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private FoldersGridAdapter() {
        //
    }

    /// ============================================================================================
    /// FoldersGridAdapterListener Interface
    /// ============================================================================================

    public interface FoldersGridAdapterListener {
        void onItemSelect(FoldersGridAdapter adapter, Folder folder);
    }

    /// ============================================================================================
    /// ViewHolder Class
    /// ============================================================================================

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.item_folder_name_text_view)
        protected TextView mFolderNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        private void bindFolder(Folder folder) {
            mFolderNameTextView.setText(folder.getName());
        }

        @Override
        public void onClick(View view) {
            Folder folder = mFolders[getAdapterPosition()];
            mListener.onItemSelect(FoldersGridAdapter.this, folder);
        }
    }
}
