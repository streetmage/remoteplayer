package com.ekubrakov.remoteplayer.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.ekubrakov.remoteplayer.R;
import com.ekubrakov.remoteplayer.entities.Song;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ekubrakov on 30.06.17.
 */

public class SongsListAdapter extends RecyclerView.Adapter<SongsListAdapter.ViewHolder> {

    private Listener mListener;

    public SongsListAdapter(Listener listener) {
        super();
        mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mListener.onSongsListAdapterItemsCountRequest(this);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_song, parent, false);
        return new ViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Song song = mListener.onSongsListAdapterSongRequest(this, position);
        holder.bindSong(song, position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_song_name_text_view)
        TextView mSongNameTextView;
        @BindView(R.id.item_song_size_text_view)
        TextView mSongSizeTextView;
        @BindView(R.id.item_play_song_toggle_button)
        ToggleButton mPlaySongToggleButton;
        @BindView(R.id.item_download_button)
        ImageButton mDownloadButton;
        @BindView(R.id.item_song_loading_progress_bar)
        ProgressBar mSongLoadingProgressBar;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindSong(Song song, final int position) {

            boolean songDownloading = mListener.onSongsListAdapterIsSongDownloadingRequest(SongsListAdapter.this, song);
            boolean songPlaying = mListener.onSongsListAdapterIsSongPlayingsRequest(SongsListAdapter.this, song);

            mSongNameTextView.setText(song.getName());
            mSongSizeTextView.setText(getSizeInMegabytes(song.getSize()));
            mPlaySongToggleButton.setChecked(songPlaying);

            if (song.getLocalPath() != null) {
                mDownloadButton.setVisibility(View.GONE);
                mSongLoadingProgressBar.setVisibility(View.GONE);
                mSongSizeTextView.setVisibility(View.GONE);
            } else {
                mDownloadButton.setVisibility(songDownloading ? View.GONE : View.VISIBLE);
                mSongLoadingProgressBar.setVisibility(songDownloading ? View.VISIBLE : View.GONE);
                mSongSizeTextView.setVisibility(View.VISIBLE);
            }

            mPlaySongToggleButton.setOnClickListener(view1 ->
                    mListener.onSongsListAdapterItemPlayButtonClick(SongsListAdapter.this, position)
            );

            mDownloadButton.setOnClickListener(v ->
                    mListener.onSongsListAdapterItemDownloadClick(SongsListAdapter.this, position)
            );

        }

        private String getSizeInMegabytes(int size) {
            return String.format("%.2f MB", (double) size / 1000000);
        }
    }

    /// Listener

    public interface Listener {

        int onSongsListAdapterItemsCountRequest(SongsListAdapter adapter);

        Song onSongsListAdapterSongRequest(SongsListAdapter adapter, int position);

        boolean onSongsListAdapterIsSongPlayingsRequest(SongsListAdapter adapter, Song song);

        boolean onSongsListAdapterIsSongDownloadingRequest(SongsListAdapter adapter, Song song);

        void onSongsListAdapterItemPlayButtonClick(SongsListAdapter adapter, int position);

        void onSongsListAdapterItemDownloadClick(SongsListAdapter adapter, int position);
    }
}
