package com.ekubrakov.remoteplayer.activity;

import com.ekubrakov.remoteplayer.helpers.PresenterFragment;
import com.ekubrakov.remoteplayer.presenter.BasePresenter;

/**
 * Created by ekubrakov on 09.08.17.
 */

public interface PresenterActivity<P extends BasePresenter> {

    P createPresenter();

    P getPresenter();

    PresenterFragment getPresenterFragment();
}
