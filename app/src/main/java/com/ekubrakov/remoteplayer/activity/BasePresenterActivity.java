package com.ekubrakov.remoteplayer.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ekubrakov.remoteplayer.helpers.PresenterFragment;
import com.ekubrakov.remoteplayer.presenter.BasePresenter;

/**
 * Created by ekubrakov on 09.08.17.
 */

public abstract class BasePresenterActivity<P extends BasePresenter> extends AppCompatActivity implements PresenterActivity<P> {

    private static final String PRESENTER_FRAGMENT_TAG = "presenter_fragment_tag";

    private P mPresenter;

    /// ============================================================================================
    /// Activity Methods
    /// ============================================================================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().bindView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().unbindView();
    }

    /// ============================================================================================
    /// PresenterActivity Methods
    /// ============================================================================================

    public P getPresenter() {

        if (mPresenter != null) {
            return mPresenter;
        }

        PresenterFragment fragment = getPresenterFragment();
        if (fragment.isPresenterSet()) {
            mPresenter = fragment.getPresenter();
        } else {
            mPresenter = createPresenter();
            fragment.setPresenter(mPresenter);
        }

        return mPresenter;
    }

    public PresenterFragment getPresenterFragment() {
        PresenterFragment presenterFragment = (PresenterFragment) getSupportFragmentManager()
                .findFragmentByTag(PRESENTER_FRAGMENT_TAG);
        if (presenterFragment == null) {
            presenterFragment = new PresenterFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(presenterFragment, PRESENTER_FRAGMENT_TAG)
                    .commit();
        }
        return presenterFragment;
    }
}
