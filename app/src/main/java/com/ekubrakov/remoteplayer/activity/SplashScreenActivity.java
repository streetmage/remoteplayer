package com.ekubrakov.remoteplayer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.preferences.ApplicationPreferences;

/**
 * Created by ekubrakov on 29.06.17.
 */

public final class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openNextActivity();
    }

    private void openNextActivity() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (DataService.getInstance().isLoggedIn()) {
                    openActivityInNewTask(PlayListActivity.class);
                } else {
                    openActivityInNewTask(LogInActivity.class);
                }
            }
        }, 500);
    }

    private void openActivityInNewTask(Class c) {
        Intent intent = new Intent(this, c);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
