package com.ekubrakov.remoteplayer.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ekubrakov.remoteplayer.R;
import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.presenter.OAuthBrowserPresenter;
import com.ekubrakov.remoteplayer.view_interface.OAuthBrowserView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ekubrakov on 26.06.17.
 */

public final class OAuthBrowserActivity extends BaseActivity<OAuthBrowserPresenter> implements OAuthBrowserView {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    @BindView(R.id.oauth_webview)
    protected WebView mWebView;
    @BindView(R.id.oauth_browser_progress_bar)
    protected ProgressBar mProgressBar;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupView();
    }

    @Override
    public OAuthBrowserPresenter createPresenter() {
        return new OAuthBrowserPresenter(DataService.getInstance());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mWebView.restoreState(savedInstanceState);
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private void setupView() {
        setContentView(R.layout.activity_oauth_browser);
        ButterKnife.bind(this);
        setupWebViewClient();
        getPresenter().onViewSetup();
    }

    private void setupWebViewClient() {

        WebViewClient webViewClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                boolean shouldOverride;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    shouldOverride = getPresenter().onShouldOverrideUrlLoading(request.getUrl());
                } else {
                    shouldOverride = super.shouldOverrideUrlLoading(view, request);
                }
                return shouldOverride;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return getPresenter().onShouldOverrideUrlLoading(Uri.parse(url));
            }
        };

        mWebView.setWebViewClient(webViewClient);
    }

    /// ============================================================================================
    /// OAuthBrowserView Methods
    /// ============================================================================================

    public void openOAuthDialog(String url) {
        mWebView.loadUrl(url);
    }

    public void openFolderSelection() {
        Intent intent = new Intent(this, FolderSelectionActivity.class);
        intent.setAction(FolderSelectionActivity.OPEN_NEW_PLAYLIST_ACTIVITY_ACTION);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void returnToLogIn() {
        setResult(RESULT_CANCELED );
        finish();
    }

    public void showProgressIndicator(boolean needsShowing) {
        mWebView.setVisibility(needsShowing ? View.INVISIBLE : View.VISIBLE);
        mProgressBar.setVisibility(needsShowing ? View.VISIBLE : View.INVISIBLE);
    }

    public void showErrorMessage(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }
}
