package com.ekubrakov.remoteplayer.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.ekubrakov.remoteplayer.notification_center.NotificationCenter;
import com.ekubrakov.remoteplayer.presenter.BasePresenter;
import com.ekubrakov.remoteplayer.services.DownloadService;
import com.ekubrakov.remoteplayer.services.MusicPlayerService;

/**
 * Created by ekubrakov on 30.06.17.
 */

public abstract class BaseActivity<P extends BasePresenter> extends BasePresenterActivity<P> {

    private NotificationsReceiver mNotificationsReceiver = new NotificationsReceiver();

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupIntentFilterForReceiver();
    }

    protected void showErrorToast(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    protected void stopMusicPlayerService() {
        Intent stopMusicPlayerServiceIntent = new Intent(this, MusicPlayerService.class);
        stopMusicPlayerServiceIntent.setAction(MusicPlayerService.STOP_SERVICE_ACTION);
        startService(stopMusicPlayerServiceIntent);
    }

    protected void stopDownloadService() {
        Intent stopDownloadServiceIntent = new Intent(this, DownloadService.class);
        stopDownloadServiceIntent.setAction(DownloadService.STOP_SERVICE_ACTION);
        startService(stopDownloadServiceIntent);
    }

    protected void transitionToLogInActivity() {
        Intent transitionIntent = new Intent(this, LogInActivity.class);
        transitionIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(transitionIntent);
        finish();
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private void setupIntentFilterForReceiver() {
        IntentFilter intentFilter = new IntentFilter(NotificationCenter.RELOGIN_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mNotificationsReceiver, intentFilter);
    }

    private void handleReceivedIntent(Intent intent) {
        String action = intent.getAction();
        if (action.equals(NotificationCenter.RELOGIN_ACTION)) {
            stopDownloadService();
            stopMusicPlayerService();
            transitionToLogInActivity();
        }
    }

    /// ============================================================================================
    /// NotificationsReceiver Class
    /// ============================================================================================

    private class NotificationsReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleReceivedIntent(intent);
        }
    }
}
