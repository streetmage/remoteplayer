package com.ekubrakov.remoteplayer.activity;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.ekubrakov.remoteplayer.R;
import com.ekubrakov.remoteplayer.adapters.SongsListAdapter;
import com.ekubrakov.remoteplayer.entities.Song;
import com.ekubrakov.remoteplayer.models.PlayListModel;
import com.ekubrakov.remoteplayer.presenter.BasePresenter;
import com.ekubrakov.remoteplayer.presenter.LogInPresenter;
import com.ekubrakov.remoteplayer.services.DownloadService;
import com.ekubrakov.remoteplayer.services.MusicPlayerService;
import com.ekubrakov.remoteplayer.views.SeparatorDecoration;
import com.ekubrakov.remoteplayer.worker_fragments.PlayListWorkerFragment;

import java.util.ArrayList;

/**
 * Created by ekubrakov on 28.06.17.
 */

public final class PlayListActivity extends AppCompatActivity implements PlayListModel.Observer,
        SongsListAdapter.Listener, SearchView.OnQueryTextListener, SeekBar.OnSeekBarChangeListener {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    public static final String SHOW_SONG_PLAYBACK = "SHOW_SONG_PLAYBACK";
    public static final String SHOW_RESUME_PLAYBACK = "SHOW_RESUME_PLAYBACK";

    public static final String SONG_PLAYING_EXTRA = "SONG_PLAYING_EXTRA";

    private static final String WORKER_FRAGMENT_TAG = "worker_fragment";

    private static final int FIRST_SONG_POSITION = 0;
    private static final int SEEK_BAR_MAX_VALUE = 1000;

    private ImageButton mMenuImageButton;
    private TextView mToolbarTitleTextView;
    private DrawerLayout mDrawerLayout;
    private SwipeRefreshLayout mSongsSwipeRefreshLayout;
    private RecyclerView mSongsRecyclerView;
    private TextView mNoSongsStubTextView;
    private LinearLayout mSongNameHolder;
    private TextView mSongNameTextView;
    private SeekBar mSeekBar;
    private ToggleButton mPlaySongToggleButton;
    private ImageButton mPreviousSongButton;
    private ImageButton mNextSongButton;
    private SearchView mSearchView;

    private PlayListModel mModel;
    private SongsListAdapter mSongsListAdapter;

    private MusicPlayerReceiver mMusicPlayerReceiver = new MusicPlayerReceiver();
    private DownloaderProgressReceiver mDownloaderProgressReceiver = new DownloaderProgressReceiver();

    private boolean mMusicPlayerServiceBound;
    private MusicPlayerService.MusicPlayerServiceBinder mMusicPlayerServiceBinder;
    private ServiceConnection mMusicPlayerServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {

            mMusicPlayerServiceBinder = (MusicPlayerService.MusicPlayerServiceBinder) service;
            mMusicPlayerServiceBound = true;

            if (mMusicPlayerServiceBinder.isPlaybackInProgress()) {
                mModel.setPlayingSong(mMusicPlayerServiceBinder.getCurrentPlayingSong());
                mModel.startSongProgressUpdate();
                mPlaySongToggleButton.setChecked(true);
            }

            updatePlayingSongName();
            updatePlayingSongProgress();

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mMusicPlayerServiceBound = false;
            updatePlayingSongName();
            updatePlayingSongProgress();
        }
    };

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupView();

        setupMusicPlayerReceiver();
        setupDownloaderProgressReceiver();

        setupWorkerFragment();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (MusicPlayerService.isServiceRunning()) {
            bindToMusicPlayerService();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindFromMusicPlayerService();
    }

    @Override
    protected void onDestroy() {
        mModel.unregisterObserver(this);
        super.onDestroy();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment.getTag().equals(WORKER_FRAGMENT_TAG)) {
            PlayListWorkerFragment workerFragment = (PlayListWorkerFragment) fragment;
            mModel = workerFragment.getModel();
            mModel.registerObserver(this);
            mModel.loadPlayList();
            updateToolbarTitle();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        setupMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FolderSelectionActivity.SELECT_FOLDER_REQUEST_CODE &&
                resultCode == FolderSelectionActivity.FOLDER_CHANGED_CODE) {
            reloadData();
            updateToolbarTitle();
            mDrawerLayout.closeDrawer(Gravity.START, false);
            mSeekBar.setProgress(0);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.START)) {
            mDrawerLayout.closeDrawer(Gravity.START);
        } else {
            super.onBackPressed();
        }
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private void setupView() {

        setContentView(R.layout.activity_play_list);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.playlist_toolbar);
        setSupportActionBar(mActionBarToolbar);

        ActionBar actionBar = getSupportActionBar();
        View actionBarLeftView = getLayoutInflater().inflate(R.layout.toolbar_left_view, null);
        actionBar.setCustomView(actionBarLeftView);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        mMenuImageButton = (ImageButton) actionBarLeftView.findViewById(R.id.menu_image_button);
        mMenuImageButton.setOnClickListener(v -> mDrawerLayout.openDrawer(Gravity.START, true));

        mToolbarTitleTextView = (TextView) actionBarLeftView.findViewById(R.id.toolbar_title_text_view);
        updateToolbarTitle();

        mSongsListAdapter = new SongsListAdapter(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        mSongsSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.songs_swipe_refresh_layout);
        mSongsSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSongsSwipeRefreshLayout.setOnRefreshListener(() -> reloadData());

        mSongsRecyclerView = (RecyclerView) findViewById(R.id.songs_recycler_view);
        mSongsRecyclerView.setHasFixedSize(true);
        SeparatorDecoration separatorDecoration = new SeparatorDecoration(mSongsRecyclerView.getContext());
        mSongsRecyclerView.addItemDecoration(separatorDecoration);
        mSongsRecyclerView.setLayoutManager(layoutManager);
        mSongsRecyclerView.setAdapter(mSongsListAdapter);

        mNoSongsStubTextView = (TextView) findViewById(R.id.no_songs_stub_text_view);
        showStubIfNeeded();

        mSongNameHolder = (LinearLayout) findViewById(R.id.song_name_holder);
        mSongNameTextView = (TextView) findViewById(R.id.song_name_text_view);

        mSeekBar = (SeekBar) findViewById(R.id.progress_seek_bar);
        mSeekBar.setOnSeekBarChangeListener(this);
        mSeekBar.setEnabled(false);
        mPlaySongToggleButton = (ToggleButton) findViewById(R.id.item_play_song_toggle_button);
        mPlaySongToggleButton.setOnClickListener(v -> startPlayback());
        mPreviousSongButton = (ImageButton) findViewById(R.id.previous_song_button);
        mPreviousSongButton.setOnClickListener(v -> playPreviousSong());
        mNextSongButton = (ImageButton) findViewById(R.id.next_song_button);
        mNextSongButton.setOnClickListener(v -> playNextSong());

        mDrawerLayout = (DrawerLayout) findViewById(R.id.playlist_drawer_layout);
    }

    private void setupWorkerFragment() {
        PlayListWorkerFragment workerFragment = (PlayListWorkerFragment) getFragmentManager().
                findFragmentByTag(WORKER_FRAGMENT_TAG);
        if (workerFragment == null) {
            workerFragment = new PlayListWorkerFragment();
            getFragmentManager().beginTransaction()
                    .add(workerFragment, WORKER_FRAGMENT_TAG)
                    .commit();
        }
    }

    private void setupMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_toolbar, menu);

        MenuItem searchActionMenuItem = menu.findItem(R.id.search_menu_item);
        mSearchView = (SearchView) searchActionMenuItem.getActionView();
        mSearchView.setIconifiedByDefault(true);
        mSearchView.setFocusable(true);
        mSearchView.setIconified(false);
        mSearchView.requestFocusFromTouch();
        mSearchView.setOnQueryTextListener(this);

        String filter = mModel.getFilter();
        if (filter != null && filter.length() > 0) {
            searchActionMenuItem.expandActionView();
            mSearchView.setQuery(filter, false);
        }
    }

    private void reloadData() {
        if (!mSongsSwipeRefreshLayout.isRefreshing()) {
            mSongsSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSongsSwipeRefreshLayout.setRefreshing(true);
                }
            });
        }
        mModel.reset();
        mModel.loadPlayList();
    }

    private void startPlayback() {

        if (!mMusicPlayerServiceBound) {
            startMusicPlayerServiceWithSongAtPosition(FIRST_SONG_POSITION);
            return;
        }

        if (mMusicPlayerServiceBinder.isPlaybackInProgress()) {
            pausePlayback();
        } else {
            resumePlayback();
        }
    }

    private void startMusicPlayerServiceWithSongAtPosition(int position) {

        if (mModel.getFilteredSongs() == null || mModel.getFilteredSongs().size() == 0) {
            mPlaySongToggleButton.setChecked(false);
            return;
        }

        Intent intent = new Intent(this, MusicPlayerService.class);
        intent.putExtra(MusicPlayerService.SONGS_EXTRA, (ArrayList) mModel.getAllSongs());
        intent.putExtra(MusicPlayerService.SONG_TO_PLAY_POSITION_EXTRA, position);
        intent.setAction(MusicPlayerService.PLAY_SONGS_SERVICE_ACTION);
        startService(intent);

        bindToMusicPlayerService();
    }

    private void bindToMusicPlayerService() {
        Intent intent = new Intent(this, MusicPlayerService.class);
        bindService(intent, mMusicPlayerServiceConnection, Context.BIND_ABOVE_CLIENT);
    }

    private void unbindFromMusicPlayerService() {
        if (mMusicPlayerServiceBound) {
            unbindService(mMusicPlayerServiceConnection);
            mMusicPlayerServiceBound = false;
        }
    }

    private void pausePlayback() {
        if (mMusicPlayerServiceBound) {
            mMusicPlayerServiceBinder.pausePlayback();
        }
    }

    private void resumePlayback() {
        if (mMusicPlayerServiceBound) {
            mMusicPlayerServiceBinder.resumePlayback();
        }
    }

    private void playPreviousSong() {
        if (mMusicPlayerServiceBound) {
            mMusicPlayerServiceBinder.playPreviousSong();
        }
    }

    private void playNextSong() {
        if (mMusicPlayerServiceBound) {
            mMusicPlayerServiceBinder.playNextSong();
        }
    }

    private void showCurrentPlayingSong(Song song) {
        mModel.setPlayingSong(song);
        mPlaySongToggleButton.setChecked(true);
    }

    private void resetPlayingSong() {
        mModel.resetPlayingSong();
        mPlaySongToggleButton.setChecked(false);
    }

    private void updatePlayingSongName() {
        if (mMusicPlayerServiceBound) {
            mSongNameHolder.setVisibility(View.VISIBLE);
            mSongNameTextView.setText(mMusicPlayerServiceBinder.getCurrentPlayingSong().getName());
            mSeekBar.setEnabled(true);
        } else {
            mSongNameHolder.setVisibility(View.GONE);
            mSeekBar.setEnabled(false);
        }
    }

    private void updatePlayingSongProgress() {
        if (mMusicPlayerServiceBound) {
            double progress = mMusicPlayerServiceBinder.getCurrentPlayingSongProgress();
            mSeekBar.setProgress((int) (progress * SEEK_BAR_MAX_VALUE));
        } else {
            mSeekBar.setProgress(0);
        }
    }

    private void updateToolbarTitle() {

        if (mToolbarTitleTextView == null || mModel == null) {
            return;
        }

        String title = String.format(getResources().getString(R.string.folder_name_format), mModel.getFolderName());
        mToolbarTitleTextView.setText(title);
    }

    private void downloadSongAtPosition(int position) {

        Song song = mModel.getFilteredSongs().get(position);
        mModel.addDownloadingSong(song);

        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra(DownloadService.SONG_EXTRA_KEY, song);
        intent.setAction(DownloadService.DOWNLOAD_SONG_ACTION);
        startService(intent);

        mSongsListAdapter.notifyDataSetChanged();
    }

    private void showStubIfNeeded() {

        if (mNoSongsStubTextView == null || mModel == null) {
            return;
        }

        boolean needsToShowStub = mModel.getAllSongs().size() == 0;
        mNoSongsStubTextView.setVisibility(needsToShowStub ? View.VISIBLE : View.INVISIBLE);
    }

    /// ============================================================================================
    /// SongsListAdapter.Listener Methods
    /// ============================================================================================


    @Override
    public int onSongsListAdapterItemsCountRequest(SongsListAdapter adapter) {
        return mModel.getFilteredSongs().size();
    }

    @Override
    public Song onSongsListAdapterSongRequest(SongsListAdapter adapter, int position) {
        return mModel.getFilteredSongs().get(position);
    }

    @Override
    public boolean onSongsListAdapterIsSongPlayingsRequest(SongsListAdapter adapter, Song song) {
        return mModel.isSongPlaying(song);
    }

    @Override
    public boolean onSongsListAdapterIsSongDownloadingRequest(SongsListAdapter adapter, Song song) {
        return mModel.isSongDownloading(song);
    }

    @Override
    public void onSongsListAdapterItemPlayButtonClick(SongsListAdapter adapter, int position) {
        if (position == mModel.getPlayingSongPosition()) {
            pausePlayback();
        } else {
            startMusicPlayerServiceWithSongAtPosition(position);
        }
    }

    @Override
    public void onSongsListAdapterItemDownloadClick(SongsListAdapter adapter, int position) {
        downloadSongAtPosition(position);
    }

    /// ============================================================================================
    /// SearchView.OnQueryTextListener
    /// ============================================================================================

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        mModel.filterSongs(s);
        mSongsListAdapter.notifyDataSetChanged();
        return false;
    }

    /// ============================================================================================
    /// SeekBar.OnSeekBarChangeListener
    /// ============================================================================================

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        //
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mModel.stopSongProgressUpdate();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (mMusicPlayerServiceBound) {
            double progress = seekBar.getProgress() / (double) SEEK_BAR_MAX_VALUE;
            mMusicPlayerServiceBinder.setCurrentPlayingSongProgress(progress);
            if (mMusicPlayerServiceBinder.isPlaybackInProgress()) {
                mModel.startSongProgressUpdate();
            }
        } else {
            seekBar.setProgress(0);
        }
    }

    /// ============================================================================================
    /// PlayListModel.Observer Methods
    /// ============================================================================================

    @Override
    public void onPlayListLoaded(PlayListModel model) {
        mSongsSwipeRefreshLayout.setRefreshing(false);
        mSongsListAdapter.notifyDataSetChanged();
        showStubIfNeeded();
    }

    @Override
    public void onPlaylistLoadingFailed(PlayListModel model, String errorMessage) {
        mSongsSwipeRefreshLayout.setRefreshing(false);
//        showErrorToast(errorMessage);
        showStubIfNeeded();
    }

    @Override
    public void onSongProgressNeedsUpdate(PlayListModel model) {
        if (mMusicPlayerServiceBound) {
            updatePlayingSongProgress();
        }
    }

    /// ============================================================================================
    /// MusicPlayerReceiver Class
    /// ============================================================================================

    private class MusicPlayerReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleMusicPlayerReceiverIntent(intent);
        }
    }

    private void setupMusicPlayerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SHOW_RESUME_PLAYBACK);
        intentFilter.addAction(SHOW_SONG_PLAYBACK);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMusicPlayerReceiver, intentFilter);
    }

    private void handleMusicPlayerReceiverIntent(Intent intent) {

        String action = intent.getAction();

        if (action.equals(SHOW_SONG_PLAYBACK)) {
            Song song = intent.getParcelableExtra(SONG_PLAYING_EXTRA);
            mModel.startSongProgressUpdate();
            showCurrentPlayingSong(song);
        } else if (action.equals(SHOW_RESUME_PLAYBACK)) {
            mModel.stopSongProgressUpdate();
            resetPlayingSong();
        }

        updatePlayingSongName();
        mSongsListAdapter.notifyDataSetChanged();
    }

    /// ============================================================================================
    /// DownloaderProgressReceiver Class
    /// ============================================================================================

    private class DownloaderProgressReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleDownloaderProgressReceiverIntent(intent);
        }
    }

    private void setupDownloaderProgressReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DownloadService.UPDATE_SONG_ACTION);
        intentFilter.addAction(DownloadService.PERFORM_AFTER_DOWNLOAD_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mDownloaderProgressReceiver, intentFilter);
    }

    private void handleDownloaderProgressReceiverIntent(Intent intent) {

        String action = intent.getAction();

        if (action.equals(DownloadService.UPDATE_SONG_ACTION) ||
                action.equals(DownloadService.PERFORM_AFTER_DOWNLOAD_ACTION)) {
            Song song = intent.getParcelableExtra(DownloadService.SONG_EXTRA_KEY);
            mModel.updateSong(song);
            mModel.removeDowloadingSong(song);
            mSongsListAdapter.notifyDataSetChanged();
        }
    }

}
