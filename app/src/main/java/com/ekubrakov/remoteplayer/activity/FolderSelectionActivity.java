package com.ekubrakov.remoteplayer.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.ekubrakov.remoteplayer.R;
import com.ekubrakov.remoteplayer.adapters.FoldersGridAdapter;
import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.entities.Folder;
import com.ekubrakov.remoteplayer.presenter.FolderSelectionPresenter;
import com.ekubrakov.remoteplayer.view_interface.FolderSelectionView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ekubrakov on 03.08.17.
 */

public class FolderSelectionActivity extends BaseActivity<FolderSelectionPresenter>
        implements FolderSelectionView, FoldersGridAdapter.FoldersGridAdapterListener {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    public static final String OPEN_NEW_PLAYLIST_ACTIVITY_ACTION = "OPEN_NEW_PLAYLIST_ACTIVITY_ACTION";
    public static final int SELECT_FOLDER_REQUEST_CODE = 999;
    public static final int FOLDER_CHANGED_CODE = 999;

    @BindView(R.id.folders_swipe_refresh_layout)
    protected SwipeRefreshLayout mFoldersSwipeRefreshLayout;
    @BindView(R.id.folders_recycler_view)
    protected RecyclerView mFoldersRecyclerView;

    private FoldersGridAdapter mFoldersGridAdapter;

    /// ============================================================================================
    /// Activity Methods
    /// ============================================================================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupView();
    }

    @Override
    public FolderSelectionPresenter createPresenter() {

        boolean needsOpenNewPlaylistActivity = false;

        Intent intent = getIntent();
        if (intent != null) {
            String action = intent.getAction();
            if (action != null && action.equals(OPEN_NEW_PLAYLIST_ACTIVITY_ACTION)) {
                needsOpenNewPlaylistActivity = true;
            }
        }

        return new FolderSelectionPresenter(DataService.getInstance(), needsOpenNewPlaylistActivity);
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private void setupView() {

        setContentView(R.layout.activity_folder_selection);

        ButterKnife.bind(this);

        mFoldersSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mFoldersSwipeRefreshLayout.setOnRefreshListener(() -> getPresenter().onForcedDataReload());

        mFoldersGridAdapter = new FoldersGridAdapter(this);
        mFoldersRecyclerView.setAdapter(mFoldersGridAdapter);

        getPresenter().onViewSetup();
    }

    /// ============================================================================================
    /// AdapterItemClickListener
    /// ============================================================================================

    public void onItemSelect(FoldersGridAdapter adapter, Folder folder) {
        getPresenter().onFolderSelected(folder);
    }

    /// ============================================================================================
    /// FolderSelectionView
    /// ============================================================================================

    public void hideHomeAsUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    public void updateFolders(Folder[] folders) {
        mFoldersGridAdapter.setFolders(folders);
    }

    public void showNewFolderSelectionConfirmationAlert() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(R.string.folder_selection)
                .setMessage(R.string.folder_selection_confirmation)
                .setPositiveButton(android.R.string.yes, ((d, w) -> getPresenter().onNewFolderSelectionConfirmed()))
                .setNegativeButton(android.R.string.no, ((d, w) -> getPresenter().onNewFolderSelectionCancelled()))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void openNewPlaylist() {
        Intent intent = new Intent(this, PlayListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void returnToPlaylist() {
        stopDownloadService();
        stopMusicPlayerService();
        setResult(FOLDER_CHANGED_CODE);
        finish();
    }

    public void showProgressIndicator(boolean needsShowing) {
        mFoldersSwipeRefreshLayout.setRefreshing(needsShowing);
    }

    public void showErrorMessage(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

}
