package com.ekubrakov.remoteplayer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ekubrakov.remoteplayer.R;
import com.ekubrakov.remoteplayer.helpers.PresenterFragment;
import com.ekubrakov.remoteplayer.presenter.BasePresenter;
import com.ekubrakov.remoteplayer.presenter.LogInPresenter;
import com.ekubrakov.remoteplayer.view_interface.LogInView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class LogInActivity extends BaseActivity<LogInPresenter> implements LogInView {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    private static final int TOKEN_REQUEST_CODE = 999;

    /// ============================================================================================
    /// Activity Methods
    /// ============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TOKEN_REQUEST_CODE:
                handleResultCode(resultCode);
            default:
                break;
        }
    }

    @Override
    public LogInPresenter createPresenter() {
        return new LogInPresenter();
    }

    @OnClick(R.id.log_in_button)
    protected void onLogInButtonClick() {
        getPresenter().onLogInButtonClick();
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private void handleResultCode(int resultCode) {
        switch (resultCode) {
            case OAuthBrowserActivity.RESULT_CANCELED:
                getPresenter().onAuthorizationFailed();
            default:
                break;
        }
    }

    /// ============================================================================================
    /// LogInView Methods
    /// ============================================================================================

    public void openOAuthBrowser() {
        Intent intent = new Intent(this, OAuthBrowserActivity.class);
        startActivityForResult(intent, TOKEN_REQUEST_CODE);
    }

    public void showAuthorizationFailedMessage() {
        Toast.makeText(this, getString(R.string.authorization_has_failed), Toast.LENGTH_SHORT).show();
    }

}
