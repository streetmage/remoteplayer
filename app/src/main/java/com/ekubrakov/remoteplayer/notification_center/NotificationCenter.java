package com.ekubrakov.remoteplayer.notification_center;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.ekubrakov.remoteplayer.application.RemotePlayerApplication;

/**
 * Created by ekubrakov on 30.06.17.
 */

public class NotificationCenter {

    public static final String RELOGIN_ACTION = "relogin_notification";

    public static void sendReloginNotification() {
        Intent intent = new Intent(RELOGIN_ACTION);
        LocalBroadcastManager.getInstance(RemotePlayerApplication.getInstance()).sendBroadcast(intent);
    }
}
