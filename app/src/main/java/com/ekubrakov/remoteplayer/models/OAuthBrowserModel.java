package com.ekubrakov.remoteplayer.models;

import android.support.annotation.Nullable;

import com.ekubrakov.remoteplayer.data_service.DataService;

/**
 * Created by ekubrakov on 10.08.17.
 */

public interface OAuthBrowserModel {
    String getAuthorizationUrl();
    void setAccessToken(String accessToken);
    void getAccountInfo(@Nullable String accountId, final DataService.AccountInfoListener listener);
    void setAccountId(String accountId);
}
