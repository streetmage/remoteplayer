package com.ekubrakov.remoteplayer.models;

import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.entities.Folder;

/**
 * Created by ekubrakov on 11.08.17.
 */

public interface FolderSelectionModel {

    void setFolderForAccount(Folder folder);

    void getFolders(final DataService.GetFoldersListener listener);
}
