package com.ekubrakov.remoteplayer.models;

import android.content.Context;
import android.database.Observable;

import com.ekubrakov.remoteplayer.R;
import com.ekubrakov.remoteplayer.entities.Folder;
import com.ekubrakov.remoteplayer.entities.Song;
import com.ekubrakov.remoteplayer.helpers.RecurrentTimer;
import com.ekubrakov.remoteplayer.preferences.ApplicationPreferences;
import com.ekubrakov.remoteplayer.data_service.DataService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * Created by ekubrakov on 29.06.17.
 */

public class PlayListModel {

    public enum State {IDLE, IN_PROGRESS, LOADING_FINISHED, LOADING_FAILED}

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    private PlayListModelModelObservable mObservable = new PlayListModelModelObservable();

    private State mState = State.IDLE;
    private Context mContext;
    private RecurrentTimer mRecurrentTimer;

    private List<Song> mAllSongs;
    private int mPlayingSongPosition = -1;

    private HashSet<Song> mDownloadingSongs = new HashSet<>();

    private String mFilter;
    private List<Song> mFilteredSongs;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public PlayListModel(Context context) {
        mContext = context;
    }

    public List<Song> getAllSongs() {
        return mAllSongs != null ? mAllSongs : new ArrayList<>();
    }

    public List<Song> getFilteredSongs() {
        return mFilteredSongs != null ? mFilteredSongs : new ArrayList<>();
    }

    public void reset() {
        mState = State.IDLE;
    }

    public void registerObserver(final PlayListModel.Observer observer) {
        mObservable.registerObserver(observer);
    }

    public void unregisterObserver(final PlayListModel.Observer observer) {
        mObservable.unregisterObserver(observer);
    }

    public void loadPlayList() {

        if (mState != State.IDLE) {
            return;
        }

        mState = State.IN_PROGRESS;

        Folder folder = DataService.getInstance().getFolderForAccount();

        if (folder == null) {
            mObservable.notifyPlaylistLoadingFailed(mContext.getResources().getString(R.string.folder_not_selected));
            return;
        }

        DataService.getInstance().getSongs(folder.getPath(), new DataService.GetSongsListener() {
            @Override
            public void onSuccess(List<Song> songs) {
                mAllSongs = songs;
                applyFilter();
                mObservable.notifyPlayListLoaded();
            }

            @Override
            public void onFailure(int responseCode, String errorMessage) {
                mObservable.notifyPlaylistLoadingFailed(errorMessage);
            }
        });

    }

    public String getFolderName() {
        Folder folder = DataService.getInstance().getFolderForAccount();
        return folder != null ? folder.getName() : new String();
    }

    public void startSongProgressUpdate() {
        if (mRecurrentTimer == null) {
            mRecurrentTimer = new RecurrentTimer();
        }
        mRecurrentTimer.setListener(t -> mObservable.notifySongProgressNeedsUpdate(this));
        mRecurrentTimer.start();
    }

    public void stopSongProgressUpdate() {
        if (mRecurrentTimer != null) {
            mRecurrentTimer.stop();
        }
    }

    public int getSongPosition(String songId) {
        int songPosition = -1;

        for (int i = 0; i < mAllSongs.size(); i++) {
            Song song = mAllSongs.get(i);
            if (songId.equals(song.getSongId())) {
                songPosition = i;
                break;
            }
        }

        return songPosition;
    }

    public void updateSong(Song song) {
        int songPosition = getSongPosition(song.getSongId());
        if (songPosition >= 0) {
            mAllSongs.set(songPosition, song);
        }
    }

    public int getPlayingSongPosition() {
        return mPlayingSongPosition;
    }

    public void setPlayingSong(Song song) {
        Optional<Song> foundSong = mAllSongs.stream().filter(p -> p.getSongId().equals(song.getSongId())).findFirst();
        if (foundSong.isPresent()) {
            mPlayingSongPosition = mAllSongs.indexOf(foundSong.get());
        }
    }

    public void resetPlayingSong() {
        mPlayingSongPosition = -1;
    }

    public void addDownloadingSong(Song song) {
        mDownloadingSongs.add(song);
    }

    public void removeDowloadingSong(Song song) {
        for (Song downloadingSong : mDownloadingSongs) {
            if (downloadingSong.getSongId().equals(song.getSongId())) {
                mDownloadingSongs.remove(downloadingSong);
                break;
            }
        }
    }

    public boolean isSongPlaying(Song song) {
        int indexOfSong = mAllSongs.indexOf(song);
        return indexOfSong >= 0 && indexOfSong == mPlayingSongPosition;
    }

    public boolean isSongDownloading(Song song) {

        boolean songDownloading = false;

        int indexOfSong = mAllSongs.indexOf(song);
        if (indexOfSong >= 0) {
            for (Song dowloadingSong : mDownloadingSongs) {
                if (dowloadingSong.getSongId().equals(song.getSongId())) {
                    songDownloading = true;
                    break;
                }
            }
        }

        return songDownloading;
    }

    public void filterSongs(final String query) {

        if (query == null) {
            return;
        }

        mFilter = query;
        applyFilter();
    }

    public String getFilter() {
        return mFilter;
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private PlayListModel() {
        //
    }

    private void applyFilter() {

        if (mAllSongs == null) {
            return;
        }

        if (mFilter != null && mFilter.length() > 0) {
            String lowercasedQuery = mFilter.toLowerCase();
            mFilteredSongs = new ArrayList<>();
            for (Song song : mAllSongs) {
                if (song.getName().toLowerCase().contains(lowercasedQuery)) {
                    mFilteredSongs.add(song);
                }
            }
        } else {
            mFilteredSongs = mAllSongs;
        }
    }

    /// ============================================================================================
    /// Observer Interface
    /// ============================================================================================

    public interface Observer {
        void onPlayListLoaded(PlayListModel model);

        void onPlaylistLoadingFailed(PlayListModel model, String errorMessage);

        void onSongProgressNeedsUpdate(PlayListModel model);
    }

    /// ============================================================================================
    /// Observable Class
    /// ============================================================================================

    private class PlayListModelModelObservable extends Observable<Observer> {

        public void notifyPlayListLoaded() {
            mState = State.LOADING_FINISHED;
            for (final Observer observer : mObservers) {
                observer.onPlayListLoaded(PlayListModel.this);
            }
        }

        public void notifyPlaylistLoadingFailed(String errorMessage) {
            mState = State.LOADING_FAILED;
            for (final Observer observer : mObservers) {
                observer.onPlaylistLoadingFailed(PlayListModel.this, errorMessage);
            }
        }

        public void notifySongProgressNeedsUpdate(PlayListModel model) {
            for (final Observer observer : mObservers) {
                observer.onSongProgressNeedsUpdate(PlayListModel.this);
            }
        }
    }

}
