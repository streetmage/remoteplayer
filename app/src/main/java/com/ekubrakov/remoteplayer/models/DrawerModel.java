package com.ekubrakov.remoteplayer.models;

import android.content.Context;
import android.database.Observable;
import android.widget.Toast;

import com.ekubrakov.remoteplayer.BuildConfig;
import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.entities.Account;
import com.ekubrakov.remoteplayer.preferences.ApplicationPreferences;

/**
 * Created by ekubrakov on 02.08.17.
 */

public class DrawerModel {

    public enum State {IDLE, IN_PROGRESS, LOADING_FINISHED, LOADING_FAILED}

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    private State mState = State.IDLE;

    private Context mContext;

    private DrawerModelObservable mObservable = new DrawerModelObservable();

    private Account mAccount;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public int getVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    public String getVersionName() {
        return BuildConfig.VERSION_NAME;
    }

    public Account getAccount() {
        return mAccount;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void loadAccountInfo() {

        if (mState != State.IDLE) {
            return;
        }

        mState = State.IN_PROGRESS;

        String accountId = DataService.getInstance().getAccountId();

        DataService.getInstance().getAccountInfo(accountId, new DataService.AccountInfoListener() {
            @Override
            public void onSuccess(Account account) {
                mState = State.LOADING_FINISHED;
                mAccount = account;
                mObservable.notifyAccountInfoLoaded();
            }

            @Override
            public void onFailure(int responseCode, String errorMessage) {
                mState = State.LOADING_FAILED;
                mObservable.notifyAccountInfoLoadingFailed(errorMessage);
            }
        });
    }

    public void registerObserver(Observer observer) {
        mObservable.registerObserver(observer);
    }

    public void unregisterObserver(Observer observer) {
        mObservable.unregisterObserver(observer);
    }

    /// ============================================================================================
    /// Observer Interface
    /// ============================================================================================

    public interface Observer {
        void onAccountInfoLoaded(DrawerModel model);
        void onAccountInfoLoadingFailed(DrawerModel model, String errorMessage);
    }

    /// ============================================================================================
    /// DrawerModelObservable Class
    /// ============================================================================================

    private class DrawerModelObservable extends Observable<Observer> {
        public void notifyAccountInfoLoaded() {
            for (final Observer observer : mObservers) {
                observer.onAccountInfoLoaded(DrawerModel.this);
            }
        }

        public void notifyAccountInfoLoadingFailed(String errorMessage) {
            for (final Observer observer : mObservers) {
                observer.onAccountInfoLoadingFailed(DrawerModel.this, errorMessage);
            }
        }
    }
}
