package com.ekubrakov.remoteplayer.models;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import com.ekubrakov.remoteplayer.data_service.DataService;
import com.ekubrakov.remoteplayer.entities.Song;
import com.ekubrakov.remoteplayer.preferences.ApplicationPreferences;
import com.ekubrakov.remoteplayer.services.MusicPlayerService;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * Created by ekubrakov on 25.07.17.
 */

public class MusicPlayerModel {

    /// ============================================================================================
    /// Properties
    /// ============================================================================================

    private Context mContext;

    private List<Song> mSongs;
    private int mCurrentPlayingSongPosition = -1;

    private MediaPlayer mMediaPlayer = new MediaPlayer();
    private boolean mPlaybackInProgress;
    private boolean mPrepared;

    private Listener mListener;

    /// ============================================================================================
    /// Public Methods
    /// ============================================================================================

    public MusicPlayerModel(Context context, Listener listener) {
        mContext = context;
        mListener = listener;
    }

    public void setSongs(List<Song> songs) {
        mSongs = songs;
    }

    public void updateSong(Song song) {

        if (mSongs == null) {
            return;
        }

        Optional<Song> foundSong = mSongs.stream().filter(p -> p.getSongId().equals(song.getSongId())).findFirst();
        if (foundSong.isPresent()) {
            int position = mSongs.indexOf(foundSong);
            mSongs.set(position, song);
        }
    }

    public void playSong(int position) {

        if (position >= 0 && position < mSongs.size()) {

            mCurrentPlayingSongPosition = position;
            Song song = mSongs.get(mCurrentPlayingSongPosition);

            if (song.getLocalPath() != null) {
                startSongPlayback(song.getLocalPath());
            } else if (song.getDownloadPath() != null) {
                startSongPlayback(song.getDownloadPath());
            } else {
                obtainSongUrlAndStartPlayback(song);
            }

            mPlaybackInProgress = true;

        } else {
            mPlaybackInProgress = false;
        }
    }

    public void pausePlayback() {
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
        }
        mPlaybackInProgress = false;
    }

    public void resumePlayback() {
        if (!mMediaPlayer.isPlaying()) {
            mMediaPlayer.start();
            mListener.onMusicPlayerModelChangedSong(this);
        }
        mPlaybackInProgress = true;
    }

    public void playPreviousSong() {

        if (mSongs == null) {
            return;
        }

        int previousPosition = mCurrentPlayingSongPosition - 1;
        if (previousPosition < 0) {
            previousPosition = mSongs.size() - 1;
        }
        playSong(previousPosition);
    }

    public void playNextSong() {

        if (mSongs == null) {
            return;
        }

        int nextPosition = mCurrentPlayingSongPosition + 1;
        if (nextPosition >= mSongs.size()) {
            nextPosition = 0;
        }
        playSong(nextPosition);
    }

    public void stopPlaying() {
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
            mMediaPlayer.stop();
            mPrepared = false;
        }
        mPlaybackInProgress = false;
    }

    public double getCurrentPlayingSongProgress() {
        double progress = 0;
        if (mPrepared && mMediaPlayer.getDuration() > 0) {
            progress = mMediaPlayer.getCurrentPosition() / (double)mMediaPlayer.getDuration();
        }
        return progress;
    }

    public void setCurrentPlayingSongProgress(double progress) {
        if (mPrepared && mMediaPlayer.getDuration() > 0) {
            int position = (int) (progress * mMediaPlayer.getDuration());
            mMediaPlayer.seekTo(position);
        }
    }

    public boolean isPlaybackInProgress() {
        return mPlaybackInProgress;
    }

    public Song getCurrentPlayingSong() {
        return mSongs.get(mCurrentPlayingSongPosition);
    }

    /// ============================================================================================
    /// Private Methods
    /// ============================================================================================

    private MusicPlayerModel() {
        //
    }

    private void obtainSongUrlAndStartPlayback(Song song) {

        DataService.getInstance().getDownloadPathForSong(song, new DataService.GetDownloadResourceUrlListener() {
            @Override
            public void onSuccess(Song song) {
                playSong(mCurrentPlayingSongPosition);
            }

            @Override
            public void onFailure(int responseCode, String errorMessage) {
                playNextSong();
            }
        });
    }

    private void startSongPlayback(String songUrl) {
        try {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
                mMediaPlayer.stop();
            }

            mMediaPlayer.reset();
            mPrepared = false;

            Uri songUri = Uri.parse(songUrl);
            String accessToken = DataService.getInstance().getAccessToken();
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", String.format("OAuth %s", accessToken));

            mMediaPlayer.setDataSource(mContext, songUri, headers);
            mMediaPlayer.setOnPreparedListener(p -> {
                if (mPlaybackInProgress) {
                    mMediaPlayer.start();
                }
                mPrepared = true;
            });
            mMediaPlayer.setOnCompletionListener(p -> playNextSong());
            mMediaPlayer.prepareAsync();

            mListener.onMusicPlayerModelChangedSong(this);

        } catch (Exception e) {
            playNextSong();
        }
    }

    ///=============================================================================================
    /// Listener
    ///=============================================================================================

    public interface Listener {
        void onMusicPlayerModelChangedSong(MusicPlayerModel model);
    }
}
